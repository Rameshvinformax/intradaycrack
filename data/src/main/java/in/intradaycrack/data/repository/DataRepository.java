package in.intradaycrack.data.repository;

import java.util.List;

import javax.inject.Inject;

import in.intradaycrack.data.PerActivity;
import in.intradaycrack.data.entity.mapper.StatusAndMessageMapper;
import in.intradaycrack.data.entity.mapper.StockTipListMapper;
import in.intradaycrack.data.entity.model.StatusAndMessageInResponse;
import in.intradaycrack.data.entity.model.StockTipListDataEntity;
import in.intradaycrack.data.net.RestApi;
import in.intradaycrack.domain.entity.StatusAndMessageDomainResponse;
import in.intradaycrack.domain.entity.StockTipListDomainEntity;
import in.intradaycrack.domain.repository.DomainRepository;
import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by Admin on 09-11-2017.
 */

@PerActivity
public class DataRepository implements DomainRepository {

    RestApi restApi;

    @Inject
    public DataRepository(RestApi restApi) {
        this.restApi = restApi;
    }

    @Override
    public Observable<List<String>> getTopStories() {
        return restApi.getTopStories();
    }

    @Override
    public Observable<StockTipListDomainEntity> getStockTipList(int pageNo) {
        return restApi.getStockTipList(pageNo).map(new Function<StockTipListDataEntity, StockTipListDomainEntity>() {
            @Override
            public StockTipListDomainEntity apply(StockTipListDataEntity stockTipListDataEntity) throws Exception {
                return new StockTipListMapper().transform(stockTipListDataEntity);
            }
    });
    }

    @Override
    public Observable<StatusAndMessageDomainResponse> updateFCMTocken(String imeiNo, String token) {
        return restApi.updateFCMToken(imeiNo,token).map(new Function<StatusAndMessageInResponse, StatusAndMessageDomainResponse>() {
            @Override
            public StatusAndMessageDomainResponse apply(StatusAndMessageInResponse statusAndMessageInResponse) throws Exception {
                return new StatusAndMessageMapper().transform(statusAndMessageInResponse);
            }
        });
    }

    @Override
    public Observable<StatusAndMessageDomainResponse> updateTipReadStatus(String stockTipId) {
        return restApi.updateStockTipview(stockTipId).map(new Function<StatusAndMessageInResponse, StatusAndMessageDomainResponse>() {
            @Override
            public StatusAndMessageDomainResponse apply(StatusAndMessageInResponse statusAndMessageInResponse) throws Exception {
                return new StatusAndMessageMapper().transform(statusAndMessageInResponse);
            }
        });
    }


}
