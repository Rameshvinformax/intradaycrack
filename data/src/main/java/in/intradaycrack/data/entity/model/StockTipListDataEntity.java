package in.intradaycrack.data.entity.model;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StockTipListDataEntity implements Serializable {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("stockTipList")
    @Expose
    private List<StockTipListDetailsData> stockTipList = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<StockTipListDetailsData> getStockTipList() {
        return stockTipList;
    }

    public void setStockTipList(List<StockTipListDetailsData> stockTipList) {
        this.stockTipList = stockTipList;
    }

}