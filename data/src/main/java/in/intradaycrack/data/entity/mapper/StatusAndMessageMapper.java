package in.intradaycrack.data.entity.mapper;

import javax.inject.Inject;

import in.intradaycrack.data.entity.model.StatusAndMessageInResponse;
import in.intradaycrack.domain.entity.StatusAndMessageDomainResponse;

/**
 * Created by Admin on 13-12-2017.
 */

public class StatusAndMessageMapper {

    @Inject
    public StatusAndMessageMapper() {
    }

    public StatusAndMessageDomainResponse transform(StatusAndMessageInResponse statusAndMessageInResponse) {
        StatusAndMessageDomainResponse response = new StatusAndMessageDomainResponse();
        response.setMessage(statusAndMessageInResponse.getMessage());
        response.setStatus(statusAndMessageInResponse.getStatus());
        response.setVersion(statusAndMessageInResponse.getVersion());
        response.setDisclimer(statusAndMessageInResponse.getDisclimer());
        return response;
    }
}