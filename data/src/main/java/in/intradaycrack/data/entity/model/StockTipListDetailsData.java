package in.intradaycrack.data.entity.model;

/**
 * Created by Admin on 29-11-2017.
 */

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StockTipListDetailsData implements Serializable {

    @SerializedName("StockTipId")
    @Expose
    private Integer stockTipId;
    @SerializedName("StockTipTitle")
    @Expose
    private String stockTipTitle;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("CallType")
    @Expose
    private String callType;
    @SerializedName("StockCategory")
    @Expose
    private String stockCategory;
    @SerializedName("StockName")
    @Expose
    private String stockName;
    @SerializedName("StockMarketPrice")
    @Expose
    private String stockMarketPrice;
    @SerializedName("BuySellRecommandPrice")
    @Expose
    private String buySellRecommandPrice;
    @SerializedName("Targets")
    @Expose
    private String targets;
    @SerializedName("StopLossPrice")
    @Expose
    private String stopLossPrice;
    @SerializedName("DateofCall")
    @Expose
    private String dateofCall;
    @SerializedName("Tipview")
    @Expose
    private Integer tipview;
    @SerializedName("Active")
    @Expose
    private Boolean active;
    private final static long serialVersionUID = -1650558155251182335L;

    public Integer getStockTipId() {
        return stockTipId;
    }

    public void setStockTipId(Integer stockTipId) {
        this.stockTipId = stockTipId;
    }

    public String getStockTipTitle() {
        return stockTipTitle;
    }

    public void setStockTipTitle(String stockTipTitle) {
        this.stockTipTitle = stockTipTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCallType() {
        return callType;
    }

    public void setCallType(String callType) {
        this.callType = callType;
    }

    public String getStockCategory() {
        return stockCategory;
    }

    public void setStockCategory(String stockCategory) {
        this.stockCategory = stockCategory;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getStockMarketPrice() {
        return stockMarketPrice;
    }

    public void setStockMarketPrice(String stockMarketPrice) {
        this.stockMarketPrice = stockMarketPrice;
    }

    public String getBuySellRecommandPrice() {
        return buySellRecommandPrice;
    }

    public void setBuySellRecommandPrice(String buySellRecommandPrice) {
        this.buySellRecommandPrice = buySellRecommandPrice;
    }

    public String getTargets() {
        return targets;
    }

    public void setTargets(String targets) {
        this.targets = targets;
    }

    public String getStopLossPrice() {
        return stopLossPrice;
    }

    public void setStopLossPrice(String stopLossPrice) {
        this.stopLossPrice = stopLossPrice;
    }

    public String getDateofCall() {
        return dateofCall;
    }

    public void setDateofCall(String dateofCall) {
        this.dateofCall = dateofCall;
    }

    public Integer getTipview() {
        return tipview;
    }

    public void setTipview(Integer tipview) {
        this.tipview = tipview;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

}