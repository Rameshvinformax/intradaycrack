package in.intradaycrack.data.entity.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by wdsi on 30-08-2017.
 */

public class ServerError {
    @SerializedName("errorCode")
    @Expose
    private String errorCode;
    @SerializedName("throwable")
    @Expose
    private Object throwable;
    @SerializedName("message")
    @Expose
    private String message;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public Object getThrowable() {
        return throwable;
    }

    public void setThrowable(Object throwable) {
        this.throwable = throwable;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
