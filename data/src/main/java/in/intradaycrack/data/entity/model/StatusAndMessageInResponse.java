package in.intradaycrack.data.entity.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Admin on 13-12-2017.
 */

public class StatusAndMessageInResponse implements Serializable {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("versionNo")
    @Expose
    private String version;

    @SerializedName("disclimer")
    @Expose
    private String disclimer;

    public String getDisclimer() {
        return disclimer;
    }

    public void setDisclimer(String disclimer) {
        this.disclimer = disclimer;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}