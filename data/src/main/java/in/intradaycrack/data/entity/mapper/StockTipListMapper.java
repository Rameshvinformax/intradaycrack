package in.intradaycrack.data.entity.mapper;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import in.intradaycrack.data.entity.model.StockTipListDataEntity;
import in.intradaycrack.data.entity.model.StockTipListDetailsData;
import in.intradaycrack.domain.entity.StockTipList;
import in.intradaycrack.domain.entity.StockTipListDomainEntity;

/**
 * Created by Admin on 29-11-2017.
 */

public class StockTipListMapper {

    @Inject
    public StockTipListMapper() {
    }

    public StockTipListDomainEntity transform(StockTipListDataEntity stockTipListDataEntity) {
        StockTipListDomainEntity stockTipListDomainEntity = new StockTipListDomainEntity();
        stockTipListDomainEntity.setMessage(stockTipListDataEntity.getMessage());
        stockTipListDomainEntity.setStatus(stockTipListDataEntity.getStatus());
        stockTipListDomainEntity.setStockTipList(transform(stockTipListDataEntity.getStockTipList()));
        return stockTipListDomainEntity;
    }

    public List<StockTipList> transform(List<StockTipListDetailsData> getStockTipList) {
        List<StockTipList> listofTips = new ArrayList<>();
        for (StockTipListDetailsData stockTipList : getStockTipList) {
            StockTipList stockTipListdomain = new StockTipList();
            stockTipListdomain.setActive(stockTipList.getActive());
            stockTipListdomain.setBuySellRecommandPrice(stockTipList.getBuySellRecommandPrice());
            stockTipListdomain.setCallType(stockTipList.getCallType());
            stockTipListdomain.setDateofCall(stockTipList.getDateofCall());
            stockTipListdomain.setDescription(stockTipList.getDescription());
            stockTipListdomain.setStockCategory(stockTipList.getStockCategory());
            stockTipListdomain.setStockMarketPrice(stockTipList.getStockMarketPrice());
            stockTipListdomain.setStockName(stockTipList.getStockName());
            stockTipListdomain.setStockTipId(stockTipList.getStockTipId());
            stockTipListdomain.setStockTipTitle(stockTipList.getStockTipTitle());
            stockTipListdomain.setStopLossPrice(stockTipList.getStopLossPrice());
            stockTipListdomain.setTargets(stockTipList.getTargets());
            stockTipListdomain.setTipview(stockTipList.getTipview());
            listofTips.add(stockTipListdomain);
        }
        return listofTips;
    }

}
