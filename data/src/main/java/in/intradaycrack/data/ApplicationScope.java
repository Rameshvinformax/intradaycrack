package in.intradaycrack.data;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by wdsi on 15-06-2017.
 */
@Scope
@Documented
@Retention(RUNTIME)
public @interface ApplicationScope {
}
