package in.intradaycrack.data.exception;

import in.intradaycrack.domain.exception.ErrorBundle;

/**
 * Created by wdsi on 06-06-2017.
 */

public class RepositoryErrorBundle implements ErrorBundle {

    private final Exception exception;

    RepositoryErrorBundle(Exception exception) {
        this.exception = exception;
    }

    @Override
    public Exception getException() {
        return exception;
    }

    @Override
    public String getErrorMessage() {
        String message = "";
        if (this.exception != null) {
            message = this.exception.getMessage();
        }
        return message;
    }
}

