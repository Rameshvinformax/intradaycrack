package in.intradaycrack.data.exception;

/**
 * Created by wdsi on 06-06-2017.
 */

public class NetworkConnectionException extends Exception {

    public NetworkConnectionException() {
        super();
    }

    public NetworkConnectionException(final Throwable cause) {
        super(cause);
    }
}
