package in.intradaycrack.data.net;

import java.util.List;

import in.intradaycrack.data.entity.model.StatusAndMessageInResponse;
import in.intradaycrack.data.entity.model.StockTipListDataEntity;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by wdsi on 06-06-2017.
 */

public interface RestApi {

    @GET("v0/topstories.json?print=pretty")
    Observable<List<String>> getTopStories();

    @GET("Share/StockTip/GetStockTipList")
    Observable<StockTipListDataEntity> getStockTipList(@Query("pageNo") int pageNo);

    @GET("Share/StockTip/UpdateFirebaseToken")
    Observable<StatusAndMessageInResponse> updateFCMToken(@Query("imeiNo") String imeiNo, @Query("token") String token);

    @GET("Share/StockTip/UpdateStockTipview")
    Observable<StatusAndMessageInResponse> updateStockTipview(@Query("stockTipId") String stockTipId);

}
