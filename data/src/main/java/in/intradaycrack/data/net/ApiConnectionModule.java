package in.intradaycrack.data.net;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import in.intradaycrack.data.ApplicationScope;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.CallAdapter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by wdsi on 06-06-2017.
 */
@Module
public class ApiConnectionModule {

    private String BaseURL;


    public ApiConnectionModule(String BaseURL) {
        this.BaseURL = BaseURL;
    }

    @Provides
    @ApplicationScope
    OkHttpClient providesOkHttpClient() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(httpLoggingInterceptor);
        httpClient.connectTimeout(90, TimeUnit.SECONDS);
        httpClient.readTimeout(40, TimeUnit.SECONDS);
        return httpClient.build();
    }

    @Provides
    @ApplicationScope
    Gson providesGson() {
        GsonBuilder sGsonBuilder = new GsonBuilder();
        /*sGsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
        sGsonBuilder.registerTypeAdapter(Date.class, new Dateserializer());*/
        sGsonBuilder.excludeFieldsWithoutExposeAnnotation();
        return sGsonBuilder.create();
    }

    @Provides
    @ApplicationScope
    GsonConverterFactory providesGsonConverterFactory(Gson gson) {
        return GsonConverterFactory.create(gson);
    }

    @Provides
    @ApplicationScope
    CallAdapter.Factory providesCallAdapterFactory() {
        return RxJava2CallAdapterFactory.create();
    }

    @Provides
    @ApplicationScope
    Retrofit provideBaseRetrofit(OkHttpClient okHttpClient, CallAdapter.Factory factory, GsonConverterFactory gsonConverterFactory) {
        return new Retrofit.Builder()
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(factory)
                .baseUrl(BaseURL)
                .client(okHttpClient)
                .build();
    }

    @Provides
    @ApplicationScope
    RestApi providesRestAPI(Retrofit retrofit) {
        return retrofit.create(RestApi.class);
    }


    @Provides
    @ApplicationScope
    String providesBaseURL() {
        return BaseURL;
    }

}
