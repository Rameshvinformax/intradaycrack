package in.intradaycrack.data.utility;

import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;


/**
 * Created by wdsi on 01-08-2017.
 */

public class DateTimeUtils {

    static String TAG = DateTimeUtils.class.getName();

    public static String calculateDaysHoursMinutes(Date fromDate, Date toDate) {
        long diff = toDate.getTime() - fromDate.getTime();
        if (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) != 0) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.DAY_OF_MONTH, (int) (calendar.get(Calendar.DAY_OF_MONTH) - TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)));
            return new SimpleDateFormat("MM dd", Locale.US).format(calendar.getTime());
        } else if (TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS) != 0) {
            if (TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS) == 1) {
                return TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS) + " hrs ago";
            } else {
                return TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS) + " hrs ago";
            }
        } else {
            return TimeUnit.MINUTES.convert(diff, TimeUnit.MILLISECONDS) + " min ago";
        }
    }

    public static String calculateDaysHoursMinutesWithTxt(Date fromDate, Date toDate) {
        long diff = toDate.getTime() - fromDate.getTime();
        if (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) != 0) {
            long days = TimeUnit.MILLISECONDS.toDays(diff);
            return days==1? days +" day ago" : days + " days ago";
        } else if (TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS) != 0) {
            if (TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS) == 1) {
                return TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS) + " hrs ago";
            } else {
                return TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS) + " hrs ago";
            }
        } else {
            return TimeUnit.MINUTES.convert(diff, TimeUnit.MILLISECONDS) + " mins ago";
        }
    }

    public static String ConvertddMMyyyyTOyyyyMMdd(String ddMMyyyyDate) {
        try {
            if (ddMMyyyyDate != null && !ddMMyyyyDate.equals("")) {
                DateFormat originalFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
                DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                Date date = originalFormat.parse(ddMMyyyyDate);
                return targetFormat.format(date);
            } else {
                return null;
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return null;
        }
    }

    public static String getDateMinuByDate(int minus) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, minus);
        return convertDateToddMM(calendar.getTime());
    }

    public static String convertDateToServerDateFromat(Date date) {
        DateFormat serverFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH);
        return serverFormat.format(date);
    }

    public static Date convertServerDateFromatToDate(String date) {
        try {
            DateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSSZ", Locale.ENGLISH);
            return serverFormat.parse(date);
        } catch (Exception e) {
            Log.e(TAG,e.getMessage());
            return null;
        }
    }

    public static String ConvertyyyyMMddTOddMMyyyy(String ddMMyyyyDate) {
        try {
            if (ddMMyyyyDate != null && !ddMMyyyyDate.equals("")) {
                DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                DateFormat targetFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
                Date date = originalFormat.parse(ddMMyyyyDate);
                return targetFormat.format(date);
            } else {
                return null;
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return null;
        }
    }


    public static Date convetStringddMMyyyyToDate(String ddMMyyyy) {
        try {
            DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
            return format.parse(ddMMyyyy);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return null;
        }
    }

    public static Date convetStringddMMyyyyHHmmssToDate(String ddMMyyyy) {
        try {
            DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.ENGLISH);
            return format.parse(ddMMyyyy);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return null;
        }
    }

    public static Date convetStringyyyyMMddHHmmssToDate(String yyyyMMddHHmmss) {
        try {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            return format.parse(yyyyMMddHHmmss);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return null;
        }
    }

    public static Date convetStringMMddyyyyhhmmssaToDate(String MMddyyyyhhmmssa) {
        try {
            DateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a", Locale.ENGLISH);
            return format.parse(MMddyyyyhhmmssa);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return null;
        }
    }

    public static Date convetStringyyyyMMddToDate(String yyyyMMdd) {
        try {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            return format.parse(yyyyMMdd);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return null;
        }
    }

    public static String getCurrentMVCDateTime() {
        TimeZone.setDefault(TimeZone.getTimeZone("GMT+530"));
        long millis = System.currentTimeMillis();
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+530"),
                Locale.getDefault());
        Date currentLocalTime = calendar.getTime();
        DateFormat date = new SimpleDateFormat("Z");
        String localTime = date.format(currentLocalTime);
        String d = "/Date(" + millis + localTime + ")/";
        return d;
    }


    public static Date convertStringToDate(String MvcDate) {
        try {
            String timeString = MvcDate.substring(MvcDate.indexOf("(") + 1, MvcDate.indexOf(")"));
            String[] timeSegments;
            String PlusorMinus = "+";
            if (timeString.contains("+")) {
                timeSegments = timeString.split("\\+");
                PlusorMinus = "+";
            } else {
                timeSegments = timeString.split("\\-");
                PlusorMinus = "-";
            }

            long timeZoneOffSet = Long.valueOf(timeSegments[1]);
            long millis = Long.valueOf(timeSegments[0]);
            TimeZone.setDefault(TimeZone.getTimeZone("GMT" + PlusorMinus + timeZoneOffSet));

            return new Date(millis);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());

            return null;
        }

    }


    public static String convertStringToDateAndTime(String MvcDate) {
        try {
            String timeString = MvcDate.substring(MvcDate.indexOf("(") + 1, MvcDate.indexOf(")"));
            String[] timeSegments;
            String PlusorMinus = "+";
            if (timeString.contains("+")) {
                timeSegments = timeString.split("\\+");
                PlusorMinus = "+";
            } else {
                timeSegments = timeString.split("\\-");
                PlusorMinus = "-";
            }

            long timeZoneOffSet = Long.valueOf(timeSegments[1]);
            long millis = Long.valueOf(timeSegments[0]);
            TimeZone.setDefault(TimeZone.getTimeZone("GMT" + PlusorMinus + timeZoneOffSet));
            DateFormat targetFormat = new SimpleDateFormat("dd-MM-yyyy&hh:mm a", Locale.ENGLISH);
            return targetFormat.format(new Date(millis));
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());

            return null;
        }

    }

    public static String getCurrentDateTimeServerDateTime() {
        TimeZone.setDefault(TimeZone.getTimeZone("GMT+530"));
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Calendar cal = Calendar.getInstance();
        return (dateFormat.format(cal.getTime()).replaceAll(" ", "T"));
    }


    public static String ConvertyyyyMMddHHmmssTOddMMyyyyhhmma(String yyyyMMddHHmmss) {
        try {
            if (yyyyMMddHHmmss != null && !yyyyMMddHHmmss.equals("")) {
                DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
                DateFormat targetFormat = new SimpleDateFormat("dd-MM-yyyy&hh:mm a", Locale.ENGLISH);
                Date date = originalFormat.parse(yyyyMMddHHmmss);
                return targetFormat.format(date);
            } else {
                return null;
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return null;
        }
    }


    public static String ConvertMMddyyyyhhmmssaTOddMM(String yyyyMMddHHmmss) {
        try {
            if (yyyyMMddHHmmss != null && !yyyyMMddHHmmss.equals("")) {
                DateFormat originalFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a", Locale.ENGLISH);
                DateFormat targetFormat = new SimpleDateFormat("dd-MM", Locale.ENGLISH);
                Date date = originalFormat.parse(yyyyMMddHHmmss);
                return targetFormat.format(date);
            } else {
                return null;
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return null;
        }
    }


    public static String ConvertddMMyyyyTOyyyyMMddWithTime(String ddMMyyyyDate) {
        try {
            if (ddMMyyyyDate != null && !ddMMyyyyDate.equals("")) {
                DateFormat originalFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.ENGLISH);
                DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
                Date date = originalFormat.parse(ddMMyyyyDate);
                return targetFormat.format(date);
            } else {
                return null;
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return null;
        }
    }


    public static String getCurrentDateHuman() {
        TimeZone.setDefault(TimeZone.getTimeZone("GMT+530"));
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        Calendar cal = Calendar.getInstance();
        return (dateFormat.format(cal.getTime()));
    }

    public static String getCurrentDateTimeHuman() {
        TimeZone.setDefault(TimeZone.getTimeZone("GMT+530"));
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.ENGLISH);
        Calendar cal = Calendar.getInstance();
        return (dateFormat.format(cal.getTime()));
    }

    public static String getCurrentDateddMMMyyyy() {
        TimeZone.setDefault(TimeZone.getTimeZone("GMT+530"));
        DateFormat dateFormat = new SimpleDateFormat("dd MMM,yyyy", Locale.ENGLISH);
        Calendar cal = Calendar.getInstance();
        return (dateFormat.format(cal.getTime()));
    }

    public static String getCurrentDateyyyyMMdd() {
        TimeZone.setDefault(TimeZone.getTimeZone("GMT+530"));
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Calendar cal = Calendar.getInstance();
        return (dateFormat.format(cal.getTime()));
    }

    public static String convertyyyyMMddTOddMMMyyyy(String yyyyMMdd) {
        TimeZone.setDefault(TimeZone.getTimeZone("GMT+530"));
        DateFormat dateFormat = new SimpleDateFormat("dd MMM,yyyy", Locale.ENGLISH);
        return (dateFormat.format(convetStringyyyyMMddToDate(yyyyMMdd)));
    }

    public static String getCurrentDateTimeServer() {
        TimeZone.setDefault(TimeZone.getTimeZone("GMT+530"));
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Calendar cal = Calendar.getInstance();
        return (dateFormat.format(cal.getTime()));
    }

    public static String getCurrentTime() {
        TimeZone.setDefault(TimeZone.getTimeZone("GMT+530"));
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
        Calendar cal = Calendar.getInstance();
        return (dateFormat.format(cal.getTime()));
    }

    public static String getCurrentTimeHuman() {
        TimeZone.setDefault(TimeZone.getTimeZone("GMT+530"));
        DateFormat dateFormat = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
        Calendar cal = Calendar.getInstance();
        return (dateFormat.format(cal.getTime()));
    }

    public static String convertDateToyyyyMMDD(Date date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        return df.format(date);
    }

    public static String convertDateToddMM(Date date) {
        DateFormat df = new SimpleDateFormat("dd-MM", Locale.ENGLISH);
        return df.format(date);
    }

    public static int getDiffYears(Date DOB) {
        TimeZone.setDefault(TimeZone.getTimeZone("GMT+530"));
        Calendar a = getCalendar(DOB);
        Calendar b = Calendar.getInstance();
        int diff = b.get(Calendar.YEAR) - a.get(Calendar.YEAR);
        if (a.get(Calendar.MONTH) > b.get(Calendar.MONTH) ||
                (a.get(Calendar.MONTH) == b.get(Calendar.MONTH) && a.get(Calendar.DATE) > b.get(Calendar.DATE))) {
            diff--;
        }
        return diff;
    }

    public static Calendar getCalendar(Date date) {
        TimeZone.setDefault(TimeZone.getTimeZone("GMT+530"));
        Calendar cal = Calendar.getInstance(Locale.US);
        cal.setTime(date);
        return cal;
    }
}
