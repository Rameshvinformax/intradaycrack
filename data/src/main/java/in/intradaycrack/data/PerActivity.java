package in.intradaycrack.data;

import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by Admin on 09-11-2017.
 */

@Scope
@Retention(RUNTIME)
public @interface PerActivity  {
}
