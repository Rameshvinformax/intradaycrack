package in.intradaycrack.domain.entity;

/**
 * Created by Admin on 29-11-2017.
 */
 import java.util.List;

public class StockTipListDomainEntity
{
    private String status;
    private String message;
    private List<StockTipList> stockTipList = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<StockTipList> getStockTipList() {
        return stockTipList;
    }

    public void setStockTipList(List<StockTipList> stockTipList) {
        this.stockTipList = stockTipList;
    }

}
