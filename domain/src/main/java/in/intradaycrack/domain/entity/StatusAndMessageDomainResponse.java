package in.intradaycrack.domain.entity;

/**
 * Created by Admin on 13-12-2017.
 */

public class StatusAndMessageDomainResponse {
    private String status;
    private String message;
    private String version;
    private String disclimer;
    public String getDisclimer() {
        return disclimer;
    }

    public void setDisclimer(String disclimer) {
        this.disclimer = disclimer;
    }



    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
