package in.intradaycrack.domain.entity;

/**
 * Created by Admin on 29-11-2017.
 */
public class StockTipList
{
    private Integer stockTipId;
    private String stockTipTitle;
    private String description;
    private String callType;
    private String stockCategory;
    private String stockName;
    private String stockMarketPrice;
    private String buySellRecommandPrice;
    private String targets;
    private String stopLossPrice;
    private String dateofCall;
    private Integer tipview;
    private Boolean active;

    public Integer getStockTipId() {
        return stockTipId;
    }

    public void setStockTipId(Integer stockTipId) {
        this.stockTipId = stockTipId;
    }

    public String getStockTipTitle() {
        return stockTipTitle;
    }

    public void setStockTipTitle(String stockTipTitle) {
        this.stockTipTitle = stockTipTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCallType() {
        return callType;
    }

    public void setCallType(String callType) {
        this.callType = callType;
    }

    public String getStockCategory() {
        return stockCategory;
    }

    public void setStockCategory(String stockCategory) {
        this.stockCategory = stockCategory;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getStockMarketPrice() {
        return stockMarketPrice;
    }

    public void setStockMarketPrice(String stockMarketPrice) {
        this.stockMarketPrice = stockMarketPrice;
    }

    public String getBuySellRecommandPrice() {
        return buySellRecommandPrice;
    }

    public void setBuySellRecommandPrice(String buySellRecommandPrice) {
        this.buySellRecommandPrice = buySellRecommandPrice;
    }

    public String getTargets() {
        return targets;
    }

    public void setTargets(String targets) {
        this.targets = targets;
    }

    public String getStopLossPrice() {
        return stopLossPrice;
    }

    public void setStopLossPrice(String stopLossPrice) {
        this.stopLossPrice = stopLossPrice;
    }

    public String getDateofCall() {
        return dateofCall;
    }

    public void setDateofCall(String dateofCall) {
        this.dateofCall = dateofCall;
    }

    public Integer getTipview() {
        return tipview;
    }

    public void setTipview(Integer tipview) {
        this.tipview = tipview;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

}
