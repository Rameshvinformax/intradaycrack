package in.intradaycrack.domain.executor;

import io.reactivex.Scheduler;

/**
 * Created by Admin on 08-11-2017.
 */

public interface PostExecutionThread {
    Scheduler getScheduler();
}
