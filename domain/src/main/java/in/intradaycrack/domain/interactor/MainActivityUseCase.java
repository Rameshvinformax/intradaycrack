package in.intradaycrack.domain.interactor;

import java.util.List;

import javax.inject.Inject;

import in.intradaycrack.domain.executor.PostExecutionThread;
import in.intradaycrack.domain.executor.ThreadExecutor;
import in.intradaycrack.domain.repository.DomainRepository;
import io.reactivex.Observable;

/**
 * Created by Admin on 08-11-2017.
 */

public class MainActivityUseCase extends UseCase<List<String>, Void> {

    public final DomainRepository menuRepository;

    @Inject
    public MainActivityUseCase(DomainRepository menuRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.menuRepository = menuRepository;
    }

    @Override
    Observable<List<String>> buildUseCaseObservable(Void aVoid) {
        return menuRepository.getTopStories();
    }
}
