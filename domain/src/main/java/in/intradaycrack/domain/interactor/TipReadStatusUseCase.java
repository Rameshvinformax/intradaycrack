package in.intradaycrack.domain.interactor;

import javax.inject.Inject;

import in.intradaycrack.domain.entity.StatusAndMessageDomainResponse;
import in.intradaycrack.domain.executor.PostExecutionThread;
import in.intradaycrack.domain.executor.ThreadExecutor;
import in.intradaycrack.domain.repository.DomainRepository;
import io.reactivex.Observable;

/**
 * Created by Admin on 13-12-2017.
 */

public class TipReadStatusUseCase extends UseCase<StatusAndMessageDomainResponse, TipReadStatusUseCase.Params> {

    public final DomainRepository menuRepository;

    @Inject
    public TipReadStatusUseCase(DomainRepository menuRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.menuRepository = menuRepository;
    }

    @Override
    Observable<StatusAndMessageDomainResponse> buildUseCaseObservable(Params params) {
        return menuRepository.updateTipReadStatus(params.tipId);
    }

    public static class Params {
        String tipId;

        public Params(String tipId) {
            this.tipId = tipId;
        }
    }
}
