package in.intradaycrack.domain.interactor;

import java.util.List;

import javax.inject.Inject;

import in.intradaycrack.domain.entity.StockTipListDomainEntity;
import in.intradaycrack.domain.executor.PostExecutionThread;
import in.intradaycrack.domain.executor.ThreadExecutor;
import in.intradaycrack.domain.repository.DomainRepository;
import io.reactivex.Observable;

/**
 * Created by Admin on 29-11-2017.
 */

public class TipsListUseCase extends UseCase<StockTipListDomainEntity, Integer> {

    public final DomainRepository menuRepository;

    @Inject
    public TipsListUseCase(DomainRepository menuRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.menuRepository = menuRepository;
    }

    @Override
    Observable<StockTipListDomainEntity> buildUseCaseObservable(Integer pageNo) {
        return menuRepository.getStockTipList(pageNo);
    }
}
