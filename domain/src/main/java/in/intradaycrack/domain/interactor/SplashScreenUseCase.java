package in.intradaycrack.domain.interactor;

import java.util.List;

import javax.inject.Inject;

import in.intradaycrack.domain.entity.StatusAndMessageDomainResponse;
import in.intradaycrack.domain.executor.PostExecutionThread;
import in.intradaycrack.domain.executor.ThreadExecutor;
import in.intradaycrack.domain.repository.DomainRepository;
import io.reactivex.Observable;

/**
 * Created by Admin on 13-12-2017.
 */

public class SplashScreenUseCase extends UseCase<StatusAndMessageDomainResponse, SplashScreenUseCase.Params> {

    public final DomainRepository menuRepository;

    @Inject
    public SplashScreenUseCase(DomainRepository menuRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.menuRepository = menuRepository;
    }

    @Override
    Observable<StatusAndMessageDomainResponse> buildUseCaseObservable(Params param) {
        return menuRepository.updateFCMTocken(param.imeiNo, param.token);
    }

    public static class Params {
        String imeiNo;
        String token;

        public Params(String imeiNo,
                      String token) {
            this.imeiNo = imeiNo;
            this.token = token;
        }
    }
}
