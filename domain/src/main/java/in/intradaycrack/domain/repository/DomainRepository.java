package in.intradaycrack.domain.repository;

import java.util.List;

import in.intradaycrack.domain.entity.StatusAndMessageDomainResponse;
import in.intradaycrack.domain.entity.StockTipListDomainEntity;
import io.reactivex.Observable;

/**
 * Created by Admin on 09-11-2017.
 */

public interface DomainRepository {

    Observable<List<String>> getTopStories();

    Observable<StockTipListDomainEntity> getStockTipList(int pageNo);

    Observable<StatusAndMessageDomainResponse> updateFCMTocken(String imeiNo, String token);

    Observable<StatusAndMessageDomainResponse> updateTipReadStatus(String stockTipId);

}
