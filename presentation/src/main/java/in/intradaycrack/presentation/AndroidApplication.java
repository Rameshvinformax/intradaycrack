package in.intradaycrack.presentation;

import android.app.Application;

import com.google.firebase.FirebaseApp;

import in.intradaycrack.presentation.BuildConfig;
import in.intradaycrack.data.net.ApiConnectionModule;
import in.intradaycrack.presentation.internal.di.components.ApplicationComponent;
import in.intradaycrack.presentation.internal.di.components.DaggerApplicationComponent;
import in.intradaycrack.presentation.internal.di.module.ApplicationModule;


/**
 * Created by Admin on 31-10-2017.
 */

public class AndroidApplication extends Application {

    private ApplicationComponent applicationComponent;
    public static AndroidApplication androidApplication;

    public static AndroidApplication getAndroidApplication() {
        return androidApplication;
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        androidApplication = this;
        applicationComponent = DaggerApplicationComponent.builder().applicationModule(
                new ApplicationModule(this)).apiConnectionModule(
                new ApiConnectionModule(BuildConfig.SERVER_URL)).build();
        FirebaseApp.initializeApp(AndroidApplication.this);
    }
}
