package in.intradaycrack.presentation.base;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.View;

import in.intradaycrack.presentation.R;
import in.intradaycrack.presentation.databinding.DialogWithOkButtonBinding;

/**
 * Created by wdsi on 15-11-2017.
 */

public class BaseFragment extends Fragment {


    public AlertDialog showDialogWithOk(Activity activity, String title, String message, View.OnClickListener onClickListener) {
        DialogWithOkButtonBinding DAEB = DataBindingUtil.inflate(activity.getLayoutInflater(), R.layout.dialog_with_ok_button, null, false);
        final AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(activity);
        builder.setView(DAEB.getRoot());
        AlertDialog alertDialog = builder.show();
        DAEB.title.setText(title);
        DAEB.message.setText(message);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        DAEB.okView.setOnClickListener(onClickListener);
        return alertDialog;
    }

    public AlertDialog showDialogWithOk(Activity activity, String title, String message) {
        DialogWithOkButtonBinding DAEB = DataBindingUtil.inflate(activity.getLayoutInflater(), R.layout.dialog_with_ok_button, null, false);
        final AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(activity);
        builder.setView(DAEB.getRoot());
        AlertDialog alertDialog = builder.show();
        DAEB.title.setText(title);
        DAEB.message.setText(message);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
     //   DAEB.okView.setOnClickListener(onClickListener);
        return alertDialog;
    }

   /* public AlertDialog showDialogWithCancel(final Activity activity, final String title, final String message, String positiveMsg, String negativeMsg, View.OnClickListener onClickListener) {
        final DialogAddEmployeeBinding DAEB = DataBindingUtil.inflate(activity.getLayoutInflater(), R.layout.dialog_add_employee, null, false);
        final AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(activity);
        builder.setView(DAEB.getRoot());
        final AlertDialog alertDialog = builder.show();
        DAEB.title.setText(title);
        DAEB.message.setText(message);
        DAEB.positiveText.setText(positiveMsg);
        DAEB.negativeText.setText(negativeMsg);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        DAEB.positiveText.setOnClickListener(onClickListener);
        DAEB.negativeText.setOnClickListener(onClickListener);
        if (title.equals(activity.getString(R.string.service_order_updated)) || title.equals(activity.getString(R.string.user_account_added))) {
            DAEB.positiveText.setVisibility(View.GONE);
            DAEB.emptyView.setVisibility(View.GONE);
        }
        DAEB.negativeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();

                if (message.equals(activity.getString(R.string.complete_your_Inspection_List))) {
                    activity.finish();
                }

            }
        });
        return alertDialog;
    }*/

    /*public AlertDialog showDialogForDelete(final Activity activity, final String title, final String message, View.OnClickListener onClickListener) {
        DialogDeleteContactBinding DAEB = DataBindingUtil.inflate(activity.getLayoutInflater(), R.layout.dialog_delete_contact, null, false);
        final AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(activity);
        builder.setView(DAEB.getRoot());
        final AlertDialog alertDialog = builder.show();
        DAEB.title.setText(title);
        DAEB.message.setText(message);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        DAEB.delete.setOnClickListener(onClickListener);
        DAEB.Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();

            }
        });
        return alertDialog;
    }*/

}
