package in.intradaycrack.presentation.base;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;

import java.io.File;

/**
 * Created by wdsi on 26-07-2017.
 */

public abstract class BaseActivity extends AppCompatActivity {
    String versionCode;

    public static void trimCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (String aChildren : children) {
                boolean success = deleteDir(new File(dir, aChildren));
                if (!success) {
                    return false;
                }
            }
        }

        // The directory is now empty so delete it
        return dir.delete();
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        //  getUserInfo();
    }
    
   /* public void getUserInfo() {
        userLoginInfoEntity = realm.where(UserLoginInfoEntity.class).findFirst();
        userLoginInfoEntity.addChangeListener(new RealmChangeListener<UserLoginInfoEntity>() {
            @Override
            public void onChange(UserLoginInfoEntity realmModel) {
                checkSessionExpired();
                checkForUpdate();
            }
        });
        checkSessionExpired();
        checkForUpdate();
    }*/

    private void validateAppVersion() {
        try {
            PackageManager manager = getPackageManager();
            PackageInfo info = manager.getPackageInfo(getPackageName(), 0);
            versionCode = info.versionName;
        } catch (Exception ex) {
            Log.d("VerError", "ValidateAppVersion: error " + ex);
        }

    }

    public void redirectStore() {
        final String appPackageName = getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }
    /*public void checkForUpdate() {
        validateAppVersion();
        if (userLoginInfoEntity != null && userLoginInfoEntity.isValid() && userLoginInfoEntity.getVersionName() != null && versionCode != null) {
            if (!userLoginInfoEntity.getVersionName().equals(versionCode)) {
                startActivity(new Intent(this, UpdateRequestActivity.class));
                finishAffinity();
            }
        }
    }*/

   /* public void checkSessionExpired() {
        if (userLoginInfoEntity == null) {
            sessionExpired();
        }
    }*/

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            trimCache(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*public void sessionExpired() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
        WOCPreferences.clearAll();
        startActivity(new Intent(BaseActivity.this, SignInActivity.class));
        finishAffinity();
    }*/
}
