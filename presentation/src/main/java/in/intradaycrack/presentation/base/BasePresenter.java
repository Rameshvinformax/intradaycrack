package in.intradaycrack.presentation.base;

/**
 * Created by wdsi on 14-06-2017.
 */

public interface BasePresenter {
    void resume();

    void pause();

    void destroy();

    void passErrorAndCase(String Error, int Case);

    void passMessage(int drawable, int TMainMessage, int TSubMessage, int Case);
}