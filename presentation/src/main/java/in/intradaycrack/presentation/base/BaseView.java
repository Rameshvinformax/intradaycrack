package in.intradaycrack.presentation.base;

/**
 * Created by wdsi on 13-06-2017.
 */

public interface BaseView {

    void showProgressbar();

    void hideProgressBar();

    void showMessage(int drawable, int TMainMessage, int TSubMessage,int Case);

    void showError(String Error);

    void showErrorWithRetry(String Error, int Case);
}
