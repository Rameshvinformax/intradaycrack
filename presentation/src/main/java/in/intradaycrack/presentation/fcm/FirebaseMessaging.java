package in.intradaycrack.presentation.fcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import in.intradaycrack.presentation.R;

import java.util.Map;

import in.intradaycrack.presentation.view.home.activity.MainActivity;

public class FirebaseMessaging extends FirebaseMessagingService {
    private static final String TAG = "rdx";
    // private Map<String, String> datas;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "FCM Data: " + remoteMessage.getData());
        Log.d(TAG, "FCM Data: " + remoteMessage.getMessageId());
        //   datas = remoteMessage.getData();
        try {
            if (remoteMessage != null) {
                //gcm.notification.body google.message_id
                showNotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody(), remoteMessage.getMessageId());
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    public void showNotification(String title, String message, String messageId) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("messageID", messageId);
        intent.putExtra("message", message);
        intent.putExtra("notification", true);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent contentIntent = TaskStackBuilder.create(this).addNextIntentWithParentStack(intent).getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder mNotifyBuilder;
        NotificationManager mNotificationManager;
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotifyBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(title)
                .setTicker(title)
                .setContentText(message).setContentIntent(contentIntent)
                .setColor(ContextCompat.getColor(FirebaseMessaging.this, R.color.green_start))
                .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND);
        mNotifyBuilder.setContentIntent(contentIntent);
        mNotifyBuilder.setContentText(message);
        mNotifyBuilder.setAutoCancel(true);
        mNotificationManager.notify(0, mNotifyBuilder.build());
    }
}
