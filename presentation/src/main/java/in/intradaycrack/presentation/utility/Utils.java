package in.intradaycrack.presentation.utility;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.intradaycrack.presentation.R;
import in.intradaycrack.data.utility.DateTimeUtils;
import in.intradaycrack.presentation.base.LogFlag;

import static in.intradaycrack.presentation.statics.Statics.REQUEST_PERMISSION;

/**
 * Created by wdsi on 29-06-2017.
 */

public class Utils {

    static String TAG = Utils.class.getName();

    public static boolean checkAndRequestPermissions(Activity activity) {
        List<String> listPermissionsNeeded = new ArrayList<>();
        /*if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (ContextCompat.checkSelfPermission(activity, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.CAMERA);
        }
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.SEND_SMS);
        }*/
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.READ_PHONE_STATE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(activity, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_PERMISSION);
            return false;
        } else {
            return true;
        }
    }

    public static String CalculateDaysHoursMinutes(Date fromDate, Date toDate) {
        long diff = toDate.getTime() - fromDate.getTime();
        if (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) != 0) {
            if (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) == 1) {
                return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) + " Day";
            } else {
                return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) + " Days";
            }

        } else if (TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS) != 0) {
            if (TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS) == 1) {
                return TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS) + " Hour";
            } else {
                return TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS) + " Hours";
            }

        } else {
            return TimeUnit.MINUTES.convert(diff, TimeUnit.MILLISECONDS) + " Minutes";
        }
    }

 public static void showAlertOk(Context context, String message, String title, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", onClickListener);
        builder.show();
    }
  /*
    public static android.support.v7.app.AlertDialog showDialogWithOk(Activity activity, String title, String message, View.OnClickListener onClickListener) {
        DialogWithOkButtonBinding DAEB = DataBindingUtil.inflate(activity.getLayoutInflater(), R.layout.dialog_with_ok_button, null, false);
        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(activity);
        builder.setView(DAEB.getRoot());
        android.support.v7.app.AlertDialog alertDialog = builder.show();
        DAEB.title.setText(title);
        DAEB.message.setText(message);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        DAEB.okView.setOnClickListener(onClickListener);
            return alertDialog;
    }


    public static android.support.v7.app.AlertDialog showDialogWithCancel(final Activity activity, final String title, final String message, String positiveMsg, String negativeMsg, View.OnClickListener onClickListener) {
        final DialogAddEmployeeBinding DAEB = DataBindingUtil.inflate(activity.getLayoutInflater(), R.layout.dialog_add_employee, null, false);
        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(activity);
        builder.setView(DAEB.getRoot());
        final android.support.v7.app.AlertDialog alertDialog = builder.show();
        DAEB.title.setText(title);
        DAEB.message.setText(message);
        DAEB.positiveText.setText(positiveMsg);
        DAEB.negativeText.setText(negativeMsg);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        DAEB.positiveText.setOnClickListener(onClickListener);
        DAEB.negativeText.setOnClickListener(onClickListener);
        if (title.equals(activity.getString(R.string.service_order_updated)) || title.equals(activity.getString(R.string.user_account_added))) {
            DAEB.positiveText.setVisibility(View.GONE);
            DAEB.emptyView.setVisibility(View.GONE);
        }
        DAEB.negativeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();

                if (message.equals(activity.getString(R.string.complete_your_Inspection_List))) {
                    activity.finish();
                }

            }
        });
        return alertDialog;
    }*/


    public static void showAlert(Context context, String message, String title, String positive, String negative, DialogInterface.OnClickListener action) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(positive, action);
        builder.setNegativeButton(negative, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public static void showAlert(Context context, String message, String title, String positive, DialogInterface.OnClickListener action) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(positive, action);
        builder.show();
    }

    public static String getVersionInfo(AppCompatActivity activity) {
        String versionName = null;
        try {
            PackageInfo packageInfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
            versionName = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            if (LogFlag.bLogOn) Log.e(TAG,e.getMessage());
        }
        return versionName;
    }

    public static String getAndroidId(AppCompatActivity activity) {
        String androidId = null;
        try {
            androidId = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
        } catch (Exception e) {
            Log.d(TAG, "getAndroidId: " + e.getMessage());
        }
        return androidId;
    }

    public static String convertDoubleToString(Double doubleValue) {
        return doubleValue == null ? null : doubleValue.toString();
    }


    public static String convertDoubleToStringDist(Double doubleValue) {
        return doubleValue == null ? "0.0 KM" : doubleValue.toString()+" KM";
    }

    public static String convertDoubleToStringCost(Double doubleValue) {
        return doubleValue == null ? "RS 0.0" : "RS "+doubleValue.toString();
    }

    public static String convertLongToStringCost(Long longValue) {
        return longValue == null ? "RS 0.0" : "RS "+longValue.toString();
    }

    public static String convertLongToStringCount(Long longValue) {
        return longValue == null ? "0" : longValue.toString();
    }

    public static String getSaltString() {
        String SALTCHARS = "1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 6) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        return salt.toString();

    }

    public static String concadinateStringNull(String string) {
        return (string == null || string.equals("null")) ? "" : string;
    }


    public static String concadinateStringCost(String string) {
        return (string == null ||string.equals("null")) ? "RS 0.0" : "RS "+string;
    }

    public static String convertToStringCount(String string) {
        return (string == null ||string.equals("null")) ? "0" : string;
    }


    public static void getRidOfkeyboard(Activity con) {
        View view = con.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) con.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            view.clearFocus();
        }
    }

    public static String SetDialogBox(Activity activity, String message, String title, String positive, String negative, DialogInterface.OnClickListener action) {

        // Set an EditText view to get user input
        final EditText input = new EditText(activity);
        final String[] inputData = {""};

        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setView(input);
        builder.setNegativeButton(negative, action);
        builder.setPositiveButton(positive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (LogFlag.bLogOn) Log.d(TAG, "input: " + input.getEditableText().toString());

                inputData[0] = input.getEditableText().toString();
                if (LogFlag.bLogOn) Log.d(TAG, "userData: " + inputData[0]);
                dialog.dismiss();
            }
        });
        builder.show();

        if (LogFlag.bLogOn) Log.d(TAG, "userData: " + inputData[0]);
        return inputData[0];

    }

    public static boolean hasText(String string) {
        return string != null && !string.trim().equals("");
    }

    public static boolean isNumeric(String string) {
        Pattern pattern = Pattern.compile("[0-9]+");
        Matcher matcher = pattern.matcher(string.trim());
        return matcher.matches();
    }

    public static boolean datePattenMatcheryyyyMMddHHmmss(String yyyyMMddHHmmss) {
        Pattern pattern = Pattern.compile("[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}");
        Matcher matcher = pattern.matcher(yyyyMMddHHmmss.trim());
        return matcher.matches();
    }

    public static boolean datePattenMatcherddMMyyyyHHmm(String ddMMyyyyHHmm) {
        Pattern pattern = Pattern.compile("[0-9]{2}-[0-9]{2}-[0-9]{4} [0-9]{2}:[0-9]{2}");
        Matcher matcher = pattern.matcher(ddMMyyyyHHmm.trim());
        return matcher.matches();
    }

    public static boolean isValidMobileNumber(String mobileNumber) {
        Pattern pattern = Pattern.compile("[789][0-9]{9}");
        Matcher matcher = pattern.matcher(mobileNumber.trim());
        return matcher.matches();
    }

    public static boolean isValidPAN(String pan) {
        Pattern pattern = Pattern.compile("[A-Z]{5}[0-9]{4}[A-Z]");
        Matcher matcher = pattern.matcher(pan.trim());
        return matcher.matches();
    }

    public static boolean isValidPinCode(String pin) {
        Pattern pattern = Pattern.compile("[1-9][0-9]{5}");
        Matcher matcher = pattern.matcher(pin.trim());
        return matcher.matches();
    }

    public static boolean isValidAadharCard(String pin) {
        Pattern pattern = Pattern.compile("[1-9][0-9]{11}");
        Matcher matcher = pattern.matcher(pin.trim());
        return matcher.matches();
    }

    public static boolean isValidBankAccountNumber(String BankAccountNumber) {
        return BankAccountNumber != null && !BankAccountNumber.equals("");
    }

    public static boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public static void RotateImage(String FilePath) {
        try {
            BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
            bmpFactoryOptions.inJustDecodeBounds = true;
            Bitmap bm;

            int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight / (float) 800);
            int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth / (float) 1000);

            if (heightRatio > 1 || widthRatio > 1) {
                if (heightRatio > widthRatio) {
                    bmpFactoryOptions.inSampleSize = heightRatio;
                } else {
                    bmpFactoryOptions.inSampleSize = widthRatio;
                }
            }

            bmpFactoryOptions.inJustDecodeBounds = false;
            bm = BitmapFactory.decodeFile(FilePath, bmpFactoryOptions);

            Bitmap rotate;

            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            rotate = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
            FileOutputStream fOut;
            fOut = new FileOutputStream(FilePath);
            rotate.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();

        } catch (Exception e) {
            if (LogFlag.bLogOn) Log.e(TAG,e.getMessage());
        }
    }

    public static boolean isValidDOB(String DOBddMMyyyy, int diffYears) {
        try {
            SimpleDateFormat ddMMyyyy = new SimpleDateFormat("dd-MM-yyyy");
            java.util.Date date = ddMMyyyy.parse(DOBddMMyyyy);
            return DateTimeUtils.getDiffYears(date) >= diffYears;
        } catch (Exception e) {
            if (LogFlag.bLogOn) Log.e(TAG,e.getMessage());
            return false;
        }

    }

    public static int getInt(String data) {
        try {
            return Integer.parseInt(data);
        } catch (Exception e) {
            return 0;
        }
    }

    public static long getLong(String data) {
        try {
            return Long.parseLong(data);
        } catch (Exception e) {
            return 0;
        }
    }

    public static String convertlongToString(Long data) {
        return data == null ? null : data.toString();
    }

    public static double getDouble(String data) {
        try {
            return Double.parseDouble(data);
        } catch (Exception e) {
            return 0.0;
        }
    }

    public static double distance(double lat1, double lon1, double lat2, double lon2, char unit) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        if (unit == 'K') {
            dist = dist * 1.609344;
        } else if (unit == 'N') {
            dist = dist * 0.8684;
        }
        return (long) dist;
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
/*::  This function converts decimal degrees to radians             :*/
/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    public static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
/*::  This function converts radians to decimal degrees             :*/
/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    public static double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    public static boolean isEmpty(String data) {
        return data == null || data.trim().equals("");
    }

    public static String getName(String name) {
        try {
            if (name != null) {
                String[] segments = name.split(" ");
                if (segments.length > 1) {
                    return segments[0].substring(0, 1) + segments[1].substring(0, 1);
                } else {
                    return segments[0].substring(0, 2);
                }
            }
        } catch (Exception e) {
            return "N/A";
        }
        return "N/A";
    }

    public static String convertMinsToHours(Double minsValue) {
        int hours = (int) Math.round(minsValue) / 60; //since both are ints, you get an int
        int minutes = (int) Math.round(minsValue) % 60;
        String minutesStr = String.valueOf(minutes).length() > 1 ? "" + minutes : "0" + minutes;
        return hours + ":" + minutesStr;
    }


}
