package in.intradaycrack.presentation.Interface;

import java.io.Serializable;

/**
 * Created by test2 on 27/4/16.
 */
public interface ClearOperation extends Serializable {
    void clear();
}
