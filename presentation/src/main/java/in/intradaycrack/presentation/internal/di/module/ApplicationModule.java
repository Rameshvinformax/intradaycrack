package in.intradaycrack.presentation.internal.di.module;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import in.intradaycrack.data.ApplicationScope;
import in.intradaycrack.data.executor.JobExecutor;
import in.intradaycrack.domain.executor.PostExecutionThread;
import in.intradaycrack.domain.executor.ThreadExecutor;
import in.intradaycrack.presentation.AndroidApplication;
import in.intradaycrack.presentation.executor.UIThread;

/**
 * Created by Admin on 08-11-2017.
 */

@Module
public class ApplicationModule {
    private final AndroidApplication application;

    public ApplicationModule(AndroidApplication application) {
        this.application = application;
    }

    @Provides
    @ApplicationScope
    Context provideApplicationContext() {
        return this.application.getApplicationContext();
    }

    @Provides
    @ApplicationScope
    ThreadExecutor provideThreadExecutor(JobExecutor jobExecutor) {
        return jobExecutor;
    }

    @Provides
    @ApplicationScope
    PostExecutionThread providePostExecutionThread(UIThread uiThread) {
        return uiThread;
    }

   /* @Provides
    @ApplicationScope
    Realm provideRealm() {
        return Realm.getDefaultInstance();
    }
*/
/*   @Provides
   @ApplicationScope
   Navigator provideNavigator(Navigator navigator) {
       return navigator;
   }*/
}
