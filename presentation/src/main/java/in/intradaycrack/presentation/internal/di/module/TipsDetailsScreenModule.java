package in.intradaycrack.presentation.internal.di.module;

import dagger.Component;
import dagger.Module;
import dagger.Provides;
import in.intradaycrack.data.PerActivity;
import in.intradaycrack.data.repository.DataRepository;
import in.intradaycrack.domain.executor.PostExecutionThread;
import in.intradaycrack.domain.executor.ThreadExecutor;
import in.intradaycrack.domain.interactor.TipReadStatusUseCase;
import in.intradaycrack.presentation.internal.di.components.ApplicationComponent;
import in.intradaycrack.presentation.view.tipsdetails.activity.TipsListItemDetails;
import in.intradaycrack.presentation.view.tipsdetails.mvp.TipsDetailsContractor;

/**
 * Created by Admin on 14-12-2017.
 */
@Module
public class TipsDetailsScreenModule {
    private TipsDetailsContractor.View view;

    public TipsDetailsScreenModule(TipsDetailsContractor.View view) {
        this.view = view;
    }

    @Provides
    @PerActivity
    TipsDetailsContractor.View providesTipsListContractorView() {
        return view;
    }

    @Provides
    @PerActivity
    TipReadStatusUseCase providesTipReadStatusUseCase(DataRepository dataRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        return new TipReadStatusUseCase(dataRepository, threadExecutor, postExecutionThread);
    }
}
