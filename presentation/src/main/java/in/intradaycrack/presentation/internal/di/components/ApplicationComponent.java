package in.intradaycrack.presentation.internal.di.components;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import in.intradaycrack.data.ApplicationScope;
import in.intradaycrack.data.net.ApiConnectionModule;
import in.intradaycrack.data.net.RestApi;
import in.intradaycrack.domain.executor.PostExecutionThread;
import in.intradaycrack.domain.executor.ThreadExecutor;
import in.intradaycrack.presentation.internal.di.module.ApplicationModule;
//import io.realm.Realm;

/**
 * Created by Admin on 08-11-2017.
 */
@Singleton
@ApplicationScope
@Component(modules = {ApplicationModule.class, ApiConnectionModule.class})
public interface ApplicationComponent {

    //Exposed to sub-graphs.
    Context context();

    ThreadExecutor threadExecutor();

    PostExecutionThread postExecutionThread();

    RestApi restApi();

   // Realm realm();
}
