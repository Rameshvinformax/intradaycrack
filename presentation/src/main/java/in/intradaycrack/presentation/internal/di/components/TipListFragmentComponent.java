package in.intradaycrack.presentation.internal.di.components;

import dagger.Component;
import in.intradaycrack.data.PerActivity;
import in.intradaycrack.presentation.internal.di.module.TipListFragmentModule;
import in.intradaycrack.presentation.view.home.tipsfragement.TipsListFragment;

/**
 * Created by Admin on 30-11-2017.
 */

@PerActivity
@Component(modules = {TipListFragmentModule.class}, dependencies = {ApplicationComponent.class})
public interface TipListFragmentComponent {
    void inject(TipsListFragment tipsListFragment);
}