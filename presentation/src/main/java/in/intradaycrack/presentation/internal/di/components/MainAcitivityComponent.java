package in.intradaycrack.presentation.internal.di.components;

import dagger.Component;
import in.intradaycrack.data.PerActivity;
import in.intradaycrack.presentation.internal.di.module.MainAcitivityModule;
import in.intradaycrack.presentation.view.main.activity.MainActivity;

/**
 * Created by Admin on 09-11-2017.
 */
@PerActivity
@Component(modules = {MainAcitivityModule.class}, dependencies = {ApplicationComponent.class})
public interface MainAcitivityComponent {
    void inject(MainActivity mainActivity);
}