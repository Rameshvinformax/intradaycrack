package in.intradaycrack.presentation.internal.di.module;

import dagger.Module;
import dagger.Provides;
import in.intradaycrack.data.PerActivity;
import in.intradaycrack.data.repository.DataRepository;
import in.intradaycrack.domain.executor.PostExecutionThread;
import in.intradaycrack.domain.executor.ThreadExecutor;
import in.intradaycrack.domain.interactor.MainActivityUseCase;
import in.intradaycrack.presentation.view.main.mvp.MainActivityContractor;

/**
 * Created by Admin on 09-11-2017.
 */

@Module
public class MainAcitivityModule {
    private MainActivityContractor.View view;

    public MainAcitivityModule(MainActivityContractor.View view) {
        this.view = view;
    }

    @Provides
    @PerActivity
    MainActivityContractor.View providesMainContractorView() {
        return view;
    }

    @Provides
    @PerActivity
    MainActivityUseCase providesMainActivityUseCase(DataRepository dataRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        return new MainActivityUseCase(dataRepository, threadExecutor, postExecutionThread);
    }
}
