package in.intradaycrack.presentation.internal.di.module;

import dagger.Module;
import dagger.Provides;
import in.intradaycrack.data.PerActivity;
import in.intradaycrack.data.repository.DataRepository;
import in.intradaycrack.domain.executor.PostExecutionThread;
import in.intradaycrack.domain.executor.ThreadExecutor;
import in.intradaycrack.domain.interactor.TipsListUseCase;
import in.intradaycrack.presentation.view.home.tipsfragement.mvp.TipsListContractor;

/**
 * Created by Admin on 30-11-2017.
 */

@Module
public class TipListFragmentModule {
    private TipsListContractor.View view;

    public TipListFragmentModule(TipsListContractor.View view) {
        this.view = view;
    }

    @Provides
    @PerActivity
    TipsListContractor.View providesTipsListContractorView() {
        return view;
    }

    @Provides
    @PerActivity
    TipsListUseCase providesTipsListUseCase(DataRepository dataRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        return new TipsListUseCase(dataRepository, threadExecutor, postExecutionThread);
    }
}

