package in.intradaycrack.presentation.internal.di.components;

import dagger.Component;
import in.intradaycrack.data.PerActivity;
import in.intradaycrack.presentation.internal.di.module.TipsDetailsScreenModule;
import in.intradaycrack.presentation.view.tipsdetails.activity.TipsListItemDetails;

/**
 * Created by Admin on 14-12-2017.
 */

@PerActivity
@Component(modules = {TipsDetailsScreenModule.class}, dependencies = {ApplicationComponent.class})
public interface TipsDetailsScreenComponent {
    void inject(TipsListItemDetails tipsListItemDetails);
}