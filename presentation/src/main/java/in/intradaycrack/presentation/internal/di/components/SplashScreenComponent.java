package in.intradaycrack.presentation.internal.di.components;

import dagger.Component;
import in.intradaycrack.data.PerActivity;
import in.intradaycrack.presentation.internal.di.module.SplashScreenModule;
import in.intradaycrack.presentation.view.splashscreen.SplashScreenActivity;

/**
 * Created by Admin on 14-12-2017.
 */

@PerActivity
@Component(modules = {SplashScreenModule.class}, dependencies = {ApplicationComponent.class})
public interface SplashScreenComponent {
    void inject(SplashScreenActivity splashScreenActivity);
}