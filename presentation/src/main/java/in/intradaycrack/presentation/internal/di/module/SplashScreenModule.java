package in.intradaycrack.presentation.internal.di.module;

import dagger.Module;
import dagger.Provides;
import in.intradaycrack.data.PerActivity;
import in.intradaycrack.data.repository.DataRepository;
import in.intradaycrack.domain.executor.PostExecutionThread;
import in.intradaycrack.domain.executor.ThreadExecutor;
import in.intradaycrack.domain.interactor.SplashScreenUseCase;
import in.intradaycrack.presentation.view.splashscreen.mvp.SplashActivityContractor;
import in.intradaycrack.presentation.view.splashscreen.mvp.SplashActivityPresenter;

/**
 * Created by Admin on 14-12-2017.
 */
@Module
public class SplashScreenModule {
    private SplashActivityContractor.View view;

    public SplashScreenModule(SplashActivityContractor.View view) {
        this.view = view;
    }

    @Provides
    @PerActivity
    SplashActivityContractor.View providesTipsListContractorView() {
        return view;
    }

    @Provides
    @PerActivity
    SplashScreenUseCase providesSplashScreenUseCase(DataRepository dataRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        return new SplashScreenUseCase(dataRepository, threadExecutor, postExecutionThread);
    }
}
