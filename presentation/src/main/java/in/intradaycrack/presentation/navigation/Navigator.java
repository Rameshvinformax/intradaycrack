package in.intradaycrack.presentation.navigation;

import android.content.Context;

import javax.inject.Inject;

import in.intradaycrack.data.ApplicationScope;

/**
 * Created by Admin on 26-11-2017.
 */

@ApplicationScope
public class Navigator {
Context context;
    @Inject
    public Navigator(Context context) {
       this.context=context;
    }

    /**
     * Goes to the user list screen.
     *
     *
     */
   /* public void navigateToMainActivity() {
            Intent intentToLaunch = MainActivity.getCallingIntent(context);
            context.startActivity(intentToLaunch);
    }*/

    /**
     * Goes to the user details screen.
     *
     * @param context A Context needed to open the destiny activity.
     */
   /* public void navigateToUserDetails(Context context, int userId) {
        if (context != null) {
            Intent intentToLaunch = UserDetailsActivity.getCallingIntent(context, userId);
            context.startActivity(intentToLaunch);
        }
    }*/
}
