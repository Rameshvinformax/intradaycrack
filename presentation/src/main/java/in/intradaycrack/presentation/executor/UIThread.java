package in.intradaycrack.presentation.executor;


import javax.inject.Inject;

import in.intradaycrack.data.ApplicationScope;
import in.intradaycrack.domain.executor.PostExecutionThread;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * Created by Admin on 08-11-2017.
 */

@ApplicationScope
public class UIThread implements PostExecutionThread {

    @Inject
    UIThread() {}

    @Override
    public Scheduler getScheduler() {
        return AndroidSchedulers.mainThread();
    }
}
