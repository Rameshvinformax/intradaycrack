package in.intradaycrack.presentation.customwidgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import in.intradaycrack.presentation.R;
import in.intradaycrack.presentation.AndroidApplication;

/**
 * Created by wdsi on 26-09-2017.
 */

public class CustomTextView extends AppCompatTextView {
    private int defaultDimension = 0;
    private int fontName;
    private int RobotoBlack = 0;
    private int RobotoBlackItalic = 1;
    private int RobotoBold = 2;
    private int RobotoBoldItalic = 3;
    private int RobotoLight = 4;
    private int RobotoLightItalic = 5;
    private int RobotoMedium = 6;
    private int RobotoMediumItalic = 7;
    private int RobotoThin = 8;
    private int RobotoThinItalic = 9;

    public CustomTextView(Context context) {
        super(context);
        init(null, 0);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.TextViewStyle, defStyle, 0);
        fontName = a.getInt(R.styleable.TextViewStyle_customTypeFace, defaultDimension);
        Drawable drawableLeft = null;
        Drawable drawableRight = null;
        Drawable drawableBottom = null;
        Drawable drawableTop = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            drawableLeft = a.getDrawable(R.styleable.TextViewStyle_drawableLeftCompat);
            drawableRight = a.getDrawable(R.styleable.TextViewStyle_drawableRightCompat);
            drawableBottom = a.getDrawable(R.styleable.TextViewStyle_drawableBottomCompat);
            drawableTop = a.getDrawable(R.styleable.TextViewStyle_drawableTopCompat);
        } else {
            final int drawableLeftId = a.getResourceId(R.styleable.TextViewStyle_drawableLeftCompat, -1);
            final int drawableRightId = a.getResourceId(R.styleable.TextViewStyle_drawableRightCompat, -1);
            final int drawableBottomId = a.getResourceId(R.styleable.TextViewStyle_drawableBottomCompat, -1);
            final int drawableTopId = a.getResourceId(R.styleable.TextViewStyle_drawableTopCompat, -1);

            if (drawableLeftId != -1)
                drawableLeft = AppCompatResources.getDrawable(getContext(), drawableLeftId);
            if (drawableRightId != -1)
                drawableRight = AppCompatResources.getDrawable(getContext(), drawableRightId);
            if (drawableBottomId != -1)
                drawableBottom = AppCompatResources.getDrawable(getContext(), drawableBottomId);
            if (drawableTopId != -1)
                drawableTop = AppCompatResources.getDrawable(getContext(), drawableTopId);
        }
        setCompoundDrawablesWithIntrinsicBounds(drawableLeft, drawableTop, drawableRight, drawableBottom);
        a.recycle();
        AndroidApplication application = (AndroidApplication) getContext().getApplicationContext();
      /*  if (fontName == RobotoBlack) {
            setTypeface(Typeface.createFromAsset(getContext().getAssets(), "Roboto-Black.ttf"));
        } else if (fontName == RobotoBlackItalic) {
            setTypeface(Typeface.createFromAsset(getContext().getAssets(), "Roboto-BlackItalic.ttf"));
        } else if (fontName == RobotoBold) {
            setTypeface(Typeface.createFromAsset(getContext().getAssets(), "Roboto-Bold.ttf"));
        } else if (fontName == RobotoLight) {
            setTypeface(Typeface.createFromAsset(getContext().getAssets(), "Roboto-Light.ttf"));
        } else if (fontName == RobotoMedium) {
            setTypeface(Typeface.createFromAsset(getContext().getAssets(), "Roboto-Medium.ttf"));
        } else if (fontName == RobotoThin) {
            setTypeface(Typeface.createFromAsset(getContext().getAssets(), "Roboto-Thin.ttf"));
        } else if (fontName == RobotoThinItalic) {
            setTypeface(Typeface.createFromAsset(getContext().getAssets(), "Roboto-ThinItalic.ttf"));
        }*/
        setTypeface(Typeface.createFromAsset(getContext().getAssets(), "Roboto-Medium.ttf"));
    }
}
