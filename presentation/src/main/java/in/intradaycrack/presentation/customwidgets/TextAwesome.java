package in.intradaycrack.presentation.customwidgets;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.util.LruCache;


public class TextAwesome extends AppCompatTextView {

    private static LruCache<String, Typeface> sTypefaceCache = new LruCache<>(12);

    public TextAwesome(Context context) {
        super(context);
        init();
    }

    public TextAwesome(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public void init() {
        Typeface typeface = sTypefaceCache.get("FONTAWESOME");
        if (typeface == null) {
            typeface = Typeface.createFromAsset(getContext().getAssets(), "fontawesome-webfont.ttf");
            sTypefaceCache.put("FONTAWESOME", typeface);
        }
        setTypeface(typeface);
    }
}