/*
 * @Created Date 8/12/16 6:00 PM
 * @Author Raghu Manickam
 * @Version V1.0
 * @Modified 8/12/16 6:00 PM
 * @License http://www.apache.org/licenses/LICENSE-2.0
 */

package in.intradaycrack.presentation.customwidgets;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import in.intradaycrack.presentation.R;
import in.intradaycrack.presentation.Interface.ClearOperation;
import in.intradaycrack.presentation.utility.Utils;


/**
 * Created by wdsi on 08-12-2016.
 */

public class ProgressController {
    private ClearOperation clearview;
    private boolean FreeToUpload = false;
    private AppCompatActivity appCompatActivity;

    private LinearLayout ProgressView_Layout;
    private LinearLayout MessageView_Layout;
    private LinearLayout message_no_internet_Layout;
    private LinearLayout progressController_Layout;
    private LinearLayout progress_controller_message_view_Layout;
    private View MainView_View;
    private TextView ProgressMessage_Text;
    private TextView Message_to_user_Text;
    private TextView MainMessage_Text;
    private TextView SubMessage_Text;
    private CustomButton button;
    private AppCompatImageView BackgroundImage_Image;


    public ProgressController(View view, ClearOperation clearOperation, AppCompatActivity appCompatActivity) {
        Log.d("Progress Controller", "Initialize");
        this.clearview = clearOperation;
        this.appCompatActivity = appCompatActivity;
        this.MainView_View = view.findViewById(R.id.MainView);
        progressController_Layout = (LinearLayout) view.findViewById(R.id.progressController);
        ProgressView_Layout = (LinearLayout) view.findViewById(R.id.include_new_progressbar);
        MessageView_Layout = (LinearLayout) view.findViewById(R.id.message_no_data_to_show);
        progress_controller_message_view_Layout = (LinearLayout) view.findViewById(R.id.progress_controller_message_view);
        Message_to_user_Text = (TextView) view.findViewById(R.id.Message_to_user);
        message_no_internet_Layout = (LinearLayout) view.findViewById(R.id.message_no_internet);
        ProgressMessage_Text = (TextView) view.findViewById(R.id.message);
        BackgroundImage_Image = (AppCompatImageView) view.findViewById(R.id.BackgroundImage);
        MainMessage_Text = (TextView) view.findViewById(R.id.MainMessage);
        SubMessage_Text = (TextView) view.findViewById(R.id.SubMessage);
        button = (CustomButton) view.findViewById(R.id.button);
    }

    public void showProgress() {
        FreeToUpload = true;
        Utils.getRidOfkeyboard(appCompatActivity);
        Log.d("Progress Controller", "ShowProgress");
        MainView_View.setVisibility(View.GONE);
        progressController_Layout.setVisibility(View.VISIBLE);
        ProgressMessage_Text.setVisibility(View.VISIBLE);
        ProgressView_Layout.setVisibility(View.VISIBLE);
        MessageView_Layout.setVisibility(View.GONE);
        message_no_internet_Layout.setVisibility(View.GONE);
        progress_controller_message_view_Layout.setVisibility(View.GONE);
        ProgressMessage_Text.setText("Processing");
    }

    public void onSuccess() {
        FreeToUpload = true;
        Utils.getRidOfkeyboard(appCompatActivity);
        Log.d("Progress Controller", "onSuccess");
        MainView_View.setVisibility(View.VISIBLE);
        progressController_Layout.setVisibility(View.GONE);
        ProgressView_Layout.setVisibility(View.GONE);
        MessageView_Layout.setVisibility(View.GONE);
        progress_controller_message_view_Layout.setVisibility(View.GONE);
        message_no_internet_Layout.setVisibility(View.GONE);
    }

    public void onInternetFailure() {
        FreeToUpload = true;
        Utils.getRidOfkeyboard(appCompatActivity);
        Log.d("Progress Controller", "onInternetFailure");
        MainView_View.setVisibility(View.GONE);
        ProgressView_Layout.setVisibility(View.GONE);
        ProgressMessage_Text.setVisibility(View.GONE);
        MessageView_Layout.setVisibility(View.GONE);
        progress_controller_message_view_Layout.setVisibility(View.GONE);
        message_no_internet_Layout.setVisibility(View.VISIBLE);
        progressController_Layout.setVisibility(View.VISIBLE);
        message_no_internet_Layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearview.clear();
            }
        });
    }

    public void setError(String Message) {
        FreeToUpload = true;
        Utils.getRidOfkeyboard(appCompatActivity);
        Log.d("Progress Controller", "SetError");
        MainView_View.setVisibility(View.GONE);
        ProgressView_Layout.setVisibility(View.GONE);
        ProgressMessage_Text.setVisibility(View.GONE);
        MessageView_Layout.setVisibility(View.VISIBLE);
        message_no_internet_Layout.setVisibility(View.GONE);
        progressController_Layout.setVisibility(View.VISIBLE);
        progress_controller_message_view_Layout.setVisibility(View.GONE);
        Message_to_user_Text.setText(Message);
        MessageView_Layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearview.clear();
            }
        });
    }

    public void setMessage(int drawable, int TMainMessage, int TSubMessage, boolean showButton, int buttonText, View.OnClickListener clickEvent) {
        FreeToUpload = true;
        Utils.getRidOfkeyboard(appCompatActivity);
        Log.d("Progress Controller", "setDescription");
        MainView_View.setVisibility(View.GONE);
        ProgressView_Layout.setVisibility(View.GONE);
        ProgressMessage_Text.setVisibility(View.GONE);
        MessageView_Layout.setVisibility(View.GONE);
        progress_controller_message_view_Layout.setVisibility(View.VISIBLE);
        message_no_internet_Layout.setVisibility(View.GONE);
        progressController_Layout.setVisibility(View.VISIBLE);
        BackgroundImage_Image.setImageResource(drawable);
        MainMessage_Text.setText(TMainMessage);
        if (TSubMessage == 0) {
            SubMessage_Text.setText(null);
            SubMessage_Text.setVisibility(View.GONE);
        } else {
            SubMessage_Text.setText(TSubMessage);
            SubMessage_Text.setVisibility(View.VISIBLE);
        }
        if (showButton) {
            button.setVisibility(View.VISIBLE);
            button.setText(buttonText);
            button.setOnClickListener(clickEvent);
        } else {
            button.setVisibility(View.GONE);
            button.setText(null);
        }
    }

    public void setError(String Message, final ClearOperation clearOperation) {
        Log.d("Progress Controller", "SetError");
        Utils.getRidOfkeyboard(appCompatActivity);
        FreeToUpload = true;
        MainView_View.setVisibility(View.GONE);
        ProgressView_Layout.setVisibility(View.GONE);
        ProgressMessage_Text.setVisibility(View.GONE);
        progress_controller_message_view_Layout.setVisibility(View.GONE);
        MessageView_Layout.setVisibility(View.VISIBLE);
        progressController_Layout.setVisibility(View.VISIBLE);
        message_no_internet_Layout.setVisibility(View.GONE);
        Message_to_user_Text.setText(Message);
        MessageView_Layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearOperation.clear();
            }
        });
    }

    public boolean isFreeToUpload() {
        return FreeToUpload;
    }
}
