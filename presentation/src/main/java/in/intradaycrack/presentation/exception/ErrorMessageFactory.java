/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package in.intradaycrack.presentation.exception;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import java.net.ConnectException;

import in.intradaycrack.presentation.R;
import in.intradaycrack.data.entity.model.ServerError;
import in.intradaycrack.data.exception.NetworkConnectionException;
import in.intradaycrack.data.exception.UserNotFoundException;
import in.intradaycrack.presentation.base.BasePresenter;
import in.intradaycrack.presentation.base.LogFlag;
import okhttp3.ResponseBody;
import retrofit2.HttpException;


/**
 * Factory used to create error messages from an Exception as a condition.
 */
public class ErrorMessageFactory {

    private ErrorMessageFactory() {
        //empty
    }

    /**
     * Creates a String representing an error message.
     *
     * @param context   Context needed to retrieve string resources.
     * @param exception An exception used as a condition to retrieve the correct error message.
     * @return {@link String} an error message.
     */
    public static String create(Context context, Exception exception) {
        String message = context.getString(R.string.exception_message_generic);

        if (exception instanceof NetworkConnectionException) {
            message = context.getString(R.string.exception_message_no_connection);
        } else if (exception instanceof UserNotFoundException) {
            message = context.getString(R.string.exception_message_user_not_found);
        }

        return message;
    }

    public static void exceptionCatcher(Throwable exception, BasePresenter presenter, int Case) {
        try {
            if (exception instanceof ConnectException) {
                presenter.passMessage(R.drawable.networkerror_icon, R.string.no_internet, R.string.no_internet_sub_msg, Case);
            } else if (exception instanceof HttpException) {
                ResponseBody body = ((HttpException) exception).response().errorBody();
                ServerError serverError = new Gson().fromJson(body.string(), ServerError.class);
                if (serverError != null && serverError.getMessage() != null) {
                    presenter.passMessage(R.drawable.networkerror_icon, R.string.oops, R.string.please_try_again_later, Case);
                } else {
                    presenter.passMessage(R.drawable.networkerror_icon, R.string.oops, R.string.please_try_again_later, Case);
                }
            } else {
                presenter.passMessage(R.drawable.networkerror_icon, R.string.oops, R.string.please_try_again_later, Case);
            }
        } catch (Exception e) {
            if (LogFlag.bLogOn) Log.e("ErrorFactory ",e.getMessage());
            presenter.passMessage(R.drawable.networkerror_icon, R.string.oops, R.string.please_try_again_later, Case);
        }
    }
}
