package in.intradaycrack.presentation.view.tipsdetails.mvp;

import android.content.Context;

import in.intradaycrack.domain.entity.StatusAndMessageDomainResponse;
import in.intradaycrack.domain.interactor.DefaultObserver;
import in.intradaycrack.domain.interactor.TipReadStatusUseCase;
import in.intradaycrack.presentation.R;
import in.intradaycrack.presentation.exception.ErrorMessageFactory;

/**
 * Created by Admin on 14-12-2017.
 */

public class TipsDetailsModel implements TipsDetailsContractor.Model {
    TipsDetailsContractor.Presenter presenter;
    Context context;
    String TAG = TipsDetailsModel.class.getName();
    int switchCase = 0;

    public TipsDetailsModel(TipsDetailsContractor.Presenter presenter, Context context) {
        this.presenter = presenter;
        this.context = context;
    }

    @Override
    public void request(TipReadStatusUseCase tipReadStatusUseCase, String tipID) {
        switchCase = 1;
        tipReadStatusUseCase.execute(new DefaultObserver<StatusAndMessageDomainResponse>() {
            @Override
            public void onNext(StatusAndMessageDomainResponse response) {
                if (response != null && response.getStatus().equals("1")) {
                    presenter.responseFromApi(response);
                } else {
                    presenter.passErrorAndCase(response.getMessage(), switchCase);
                }
            }

            @Override
            public void onError(Throwable exception) {
                ErrorMessageFactory.exceptionCatcher(exception, presenter, switchCase);
            }
        }, new TipReadStatusUseCase.Params(tipID));
    }
}
