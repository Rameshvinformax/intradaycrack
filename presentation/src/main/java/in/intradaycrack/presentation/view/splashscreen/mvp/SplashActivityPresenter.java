package in.intradaycrack.presentation.view.splashscreen.mvp;

import android.content.Context;

import javax.inject.Inject;

import in.intradaycrack.domain.entity.StatusAndMessageDomainResponse;
import in.intradaycrack.domain.interactor.SplashScreenUseCase;

/**
 * Created by Admin on 14-12-2017.
 */

public class SplashActivityPresenter implements SplashActivityContractor.Presenter {

    Context context;
    SplashActivityModel model;
    SplashScreenUseCase splashScreenUseCase;
    SplashActivityContractor.View view;

    @Inject
    public SplashActivityPresenter(SplashScreenUseCase splashScreenUseCase, Context context, SplashActivityContractor.View view) {
        this.splashScreenUseCase = splashScreenUseCase;
        this.context = context;
        this.view = view;
        this.model = new SplashActivityModel(SplashActivityPresenter.this, context);
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void passErrorAndCase(String Error, int Case) {

    }

    @Override
    public void passMessage(int drawable, int TMainMessage, int TSubMessage, int Case) {
        view.showMessage(drawable, TMainMessage, TSubMessage, Case);
    }

    @Override
    public void requestToApi(String imeiNo, String token) {
        view.showProgressbar();
        model.request(imeiNo, token, splashScreenUseCase);
    }

    @Override
    public void responseFromApi(StatusAndMessageDomainResponse response) {
        view.hideProgressBar();
        view.setDisplay(response);
    }
}
