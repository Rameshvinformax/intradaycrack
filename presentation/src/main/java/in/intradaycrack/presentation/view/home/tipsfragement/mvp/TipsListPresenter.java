package in.intradaycrack.presentation.view.home.tipsfragement.mvp;

import android.content.Context;

import java.util.List;

import javax.inject.Inject;

import in.intradaycrack.domain.entity.StockTipListDomainEntity;
import in.intradaycrack.domain.interactor.TipsListUseCase;

/**
 * Created by Admin on 09-11-2017.
 */

public class TipsListPresenter implements TipsListContractor.Presenter {

    Context context;
    TipsListModel model;
    TipsListUseCase tipsListUseCase;
    TipsListContractor.View view;

    @Inject
    public TipsListPresenter(TipsListUseCase tipsListUseCase, Context context, TipsListContractor.View view) {
        this.tipsListUseCase = tipsListUseCase;
        this.context = context;
        this.view = view;
        this.model = new TipsListModel(TipsListPresenter.this, context);
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void passErrorAndCase(String Error, int Case) {
    }

    @Override
    public void passMessage(int drawable, int TMainMessage, int TSubMessage, int Case) {
        view.showMessage(drawable, TMainMessage, TSubMessage, Case);
    }

    @Override
    public void requestToApi(boolean showProgress, int pageNo) {
        if (showProgress) {
            view.showProgressbar();
        }
        model.request(tipsListUseCase, pageNo, showProgress);
    }

    @Override
    public void responseFromApi(boolean showProgress, StockTipListDomainEntity listOfResult) {
        if (showProgress) {
            view.hideProgressBar();
        }
        view.setDisplay(showProgress, listOfResult);
    }
}
