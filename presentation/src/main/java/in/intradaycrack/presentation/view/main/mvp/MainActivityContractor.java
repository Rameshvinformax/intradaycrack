package in.intradaycrack.presentation.view.main.mvp;

import java.util.List;

import in.intradaycrack.domain.interactor.MainActivityUseCase;
import in.intradaycrack.presentation.base.BasePresenter;
import in.intradaycrack.presentation.base.BaseView;

/**
 * Created by Admin on 09-11-2017.
 */

public interface MainActivityContractor {

    interface View extends BaseView {
        void setDisplay(List<String> listOfResult);
    }

    interface Model {
        void request(MainActivityUseCase mainActivityUseCase);
    }

    interface Presenter extends BasePresenter {
        void requestToApi();

        void responseFromApi(List<String> listOfResult);
    }
}
