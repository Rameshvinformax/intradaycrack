package in.intradaycrack.presentation.view.tipsdetails.mvp;

import android.content.Context;

import javax.inject.Inject;

import in.intradaycrack.domain.entity.StatusAndMessageDomainResponse;
import in.intradaycrack.domain.interactor.TipReadStatusUseCase;

/**
 * Created by Admin on 14-12-2017.
 */

public class TipsDetailsPresenter implements TipsDetailsContractor.Presenter{

    Context context;
    TipsDetailsModel model;
    TipReadStatusUseCase tipReadStatusUseCase;
    TipsDetailsContractor.View view;

    @Inject
    public TipsDetailsPresenter(TipReadStatusUseCase tipReadStatusUseCase, Context context, TipsDetailsContractor.View view) {
        this.tipReadStatusUseCase = tipReadStatusUseCase;
        this.context = context;
        this.view = view;
        this.model = new TipsDetailsModel(TipsDetailsPresenter.this, context);
    }
    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void passErrorAndCase(String Error, int Case) {

    }

    @Override
    public void passMessage(int drawable, int TMainMessage, int TSubMessage, int Case) {
        view.showMessage(drawable, TMainMessage, TSubMessage, Case);
    }

    @Override
    public void requestToApi(String tipID) {
        view.showProgressbar();
        model.request(tipReadStatusUseCase,tipID);
    }

    @Override
    public void responseFromApi(StatusAndMessageDomainResponse response) {
        view.hideProgressBar();
        view.setDisplay(response);
    }
}
