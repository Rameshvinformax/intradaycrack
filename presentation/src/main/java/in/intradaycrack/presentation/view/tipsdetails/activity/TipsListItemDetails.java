package in.intradaycrack.presentation.view.tipsdetails.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import javax.inject.Inject;

import in.intradaycrack.domain.entity.StatusAndMessageDomainResponse;
import in.intradaycrack.presentation.AndroidApplication;
import in.intradaycrack.presentation.Interface.ClearOperation;
import in.intradaycrack.presentation.R;
import in.intradaycrack.presentation.base.BaseActivity;
import in.intradaycrack.presentation.customwidgets.ProgressController;
import in.intradaycrack.presentation.databinding.ActivityTipsListItemDetailsBinding;
import in.intradaycrack.presentation.internal.di.components.DaggerTipListFragmentComponent;
import in.intradaycrack.presentation.internal.di.components.DaggerTipsDetailsScreenComponent;
import in.intradaycrack.presentation.internal.di.module.TipsDetailsScreenModule;
import in.intradaycrack.presentation.utility.DateTimeUtils;
import in.intradaycrack.presentation.utility.Utils;
import in.intradaycrack.presentation.view.splashscreen.SplashScreenActivity;
import in.intradaycrack.presentation.view.tipsdetails.mvp.TipsDetailsContractor;
import in.intradaycrack.presentation.view.tipsdetails.mvp.TipsDetailsPresenter;

public class TipsListItemDetails extends BaseActivity implements TipsDetailsContractor.View, ClearOperation {

    ActivityTipsListItemDetailsBinding binding;
    @Inject
    TipsDetailsPresenter presenter;
    ProgressController progressController;
    String tipId, title = "", datetime, calltype, category, stockname, disclimer, description;
    Animation animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(TipsListItemDetails.this, R.layout.activity_tips_list_item_details);
        setSupportActionBar(binding.toolbar1);
        binding.toolbar1.setNavigationIcon(android.support.v7.appcompat.R.drawable.abc_ic_ab_back_material);
        binding.toolbar1.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        DaggerTipsDetailsScreenComponent.builder().applicationComponent(AndroidApplication.getAndroidApplication().getApplicationComponent()).tipsDetailsScreenModule(new TipsDetailsScreenModule(TipsListItemDetails.this)).build().inject(TipsListItemDetails.this);
        tipId = getIntent().getStringExtra("tipId");
        title = getIntent().getStringExtra("title");
        progressController = new ProgressController(binding.getRoot(), TipsListItemDetails.this, TipsListItemDetails.this);
        binding.CollapsingToolbarLayout1.setTitle(title);
        presenter.requestToApi(tipId);
    }

    public void setView() {
        datetime = getIntent().getStringExtra("datetime");
        calltype = getIntent().getStringExtra("calltype");
        category = getIntent().getStringExtra("category");
        stockname = getIntent().getStringExtra("stockname");
        disclimer = getIntent().getStringExtra("disclimer");
        description = getIntent().getStringExtra("description");
        if (datetime != null && datetime.contains("T")) {
            String spilt[] = datetime.split("T");
            String dateTimeSplt = DateTimeUtils.ConvertddMMyyyyTOyyyyMMMdd(spilt[0]) + " " + spilt[1];
            binding.dateTime.setText("Posted date : "+dateTimeSplt);
            binding.descriptionTitle.setText("Description");
            binding.description.setText(description);
            binding.disclimerTitle.setText("Disclaimer");
            binding.disclimer.setText(disclimer);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void showProgressbar() {
        progressController.showProgress();
    }

    @Override
    public void hideProgressBar() {
        progressController.onSuccess();
    }

    @Override
    public void showMessage(int drawable, int TMainMessage, int TSubMessage, int Case) {
        switch (Case) {
            case 0:
                progressController.setMessage(drawable, TMainMessage, TSubMessage, false, 0, null);
                break;
            case 1:
                progressController.setMessage(drawable, TMainMessage, TSubMessage, true, R.string.try_again, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        presenter.requestToApi(tipId);
                    }
                });
                break;
            default:
                progressController.setMessage(drawable, TMainMessage, TSubMessage, false, 0, null);
        }
    }

    @Override
    public void showError(String Error) {
        progressController.setError(Error);
    }

    @Override
    public void showErrorWithRetry(String Error, final int Case) {
        progressController.setError(Error, new ClearOperation() {
            @Override
            public void clear() {
                switch (Case) {
                    case 1:
                        presenter.requestToApi(tipId);
                        break;
                }
            }
        });
    }

    @Override
    public void setDisplay(StatusAndMessageDomainResponse response) {
        setView();
    }

    @Override
    public void clear() {

    }
}
