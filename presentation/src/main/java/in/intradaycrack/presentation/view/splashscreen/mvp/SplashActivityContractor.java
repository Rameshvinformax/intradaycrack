package in.intradaycrack.presentation.view.splashscreen.mvp;

import in.intradaycrack.domain.entity.StatusAndMessageDomainResponse;
import in.intradaycrack.domain.interactor.SplashScreenUseCase;
import in.intradaycrack.presentation.base.BasePresenter;
import in.intradaycrack.presentation.base.BaseView;

/**
 * Created by Admin on 14-12-2017.
 */

public interface SplashActivityContractor {
    interface View extends BaseView {
        void setDisplay(StatusAndMessageDomainResponse response);
    }

    interface Model {
        void request(String imeiNo, String token, SplashScreenUseCase mainActivityUseCase);
    }

    interface Presenter extends BasePresenter {
        void requestToApi(String imeiNo, String token);

        void responseFromApi(StatusAndMessageDomainResponse response);
    }
}
