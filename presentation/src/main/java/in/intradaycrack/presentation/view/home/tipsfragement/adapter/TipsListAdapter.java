package in.intradaycrack.presentation.view.home.tipsfragement.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.intradaycrack.presentation.R;
import in.intradaycrack.presentation.databinding.ItemTipslistHomeBinding;
import in.intradaycrack.domain.entity.StockTipList;
import in.intradaycrack.presentation.utility.DateTimeUtils;
import in.intradaycrack.presentation.view.home.tipsfragement.TipsListFragment;
import in.intradaycrack.presentation.view.home.tipsfragement.mvp.TipsListContractor;
import in.intradaycrack.presentation.view.tipsdetails.activity.TipsListItemDetails;


public class TipsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    TipsListContractor.View view;
    List<StockTipList> stockTipList;
    Context context;
    String disclimer;
    private PaginationAdapterCallback mCallback;

    // View Types
    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean retryPageLoad = false;
    private boolean isLoadingAdded = false;
    private String errorMsg;
    int lastPosition = -1;


    public TipsListAdapter(List<StockTipList> stockTipList, TipsListContractor.View view, Context context, String disclimer) {
        this.stockTipList = new ArrayList<>(stockTipList);
        this.view = view;
        this.context = context;
        this.disclimer = disclimer;
        this.mCallback = (PaginationAdapterCallback) new TipsListFragment();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //  return new ViewHolder((ItemTipslistHomeBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_tipslist_home, parent, false));
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                viewHolder = new ViewHolder((ItemTipslistHomeBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_tipslist_home, parent, false));
                break;
            case LOADING:
                View viewLoading = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(viewLoading);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        StockTipList result = stockTipList.get(position); // Movie

        switch (getItemViewType(position)) {
            case ITEM:
                final ViewHolder itemHolder = (ViewHolder) holder;
                itemHolder.getItemView().txtTipstitle.setText(stockTipList.get(position).getStockTipTitle());
                itemHolder.getItemView().txtTipsmessage.setText(stockTipList.get(position).getDescription());
                if (stockTipList.get(position).getDateofCall() != null && stockTipList.get(position).getDateofCall().contains("T")) {
                   String spilt[] = stockTipList.get(position).getDateofCall().split("T");
                   String dateTime = DateTimeUtils.ConvertddMMyyyyTOyyyyMMMdd(spilt[0]) + " "+spilt[1];
                    itemHolder.getItemView().txtTipsdate.setText(dateTime);
                }
                int colorRes = R.color.green_centre;
                switch (position % 7) {
                    case 0:
                        colorRes = R.color.design_red;
                        break;
                    case 1:
                        colorRes = R.color.design_green_trip;
                        break;
                    case 2:
                        colorRes = R.color.list_selected;
                        break;
                    case 3:
                        colorRes = R.color.design_orange_trip;
                        break;
                    case 4:
                        colorRes = R.color.addlayout;
                        break;
                    case 5:
                        colorRes = R.color.pink;
                        break;
                    case 6:
                        colorRes = R.color.green_centre;
                        break;
                }
                itemHolder.getItemView().dynamicColor.setBackgroundColor(ContextCompat.getColor(context, colorRes));
                itemHolder.getItemView().getRoot().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, TipsListItemDetails.class);
                        intent.putExtra("disclimer", disclimer);
                        intent.putExtra("tipId", "" + stockTipList.get(position).getStockTipId());
                        intent.putExtra("title", "" + stockTipList.get(position).getStockTipTitle());
                        intent.putExtra("datetime", "" + stockTipList.get(position).getDateofCall());
                        intent.putExtra("calltype", "" + stockTipList.get(position).getCallType());
                        intent.putExtra("category", "" + stockTipList.get(position).getStockCategory());
                        intent.putExtra("stockname", "" + stockTipList.get(position).getStockName());
                        intent.putExtra("description", "" + stockTipList.get(position).getDescription());
                        context.startActivity(intent);
                    }
                });
                if (position > lastPosition) {

                    Animation animation = AnimationUtils.loadAnimation(context,
                            R.anim.up_from_bottom);
                    itemHolder.getItemView().getRoot().startAnimation(animation);
                    lastPosition = position;
                }
                break;
            case LOADING:
                LoadingVH loadingVH = (LoadingVH) holder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    context.getString(R.string.error_msg_unknown));

                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

/*    @Override
    public void onBindViewHolder(final TipsListAdapter.ViewHolder holder, final int position) {

    }*/


    @Override
    public int getItemCount() {
        return stockTipList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return ITEM;
        } else {
            return (position == stockTipList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
        }
    }

    public void add(StockTipList stock) {
        stockTipList.add(stock);
        notifyItemInserted(stockTipList.size() - 1);
    }

    public void addAll(List<StockTipList> stockTipList) {
        for (StockTipList result : stockTipList) {
            add(result);
        }
    }

    public void remove(StockTipList r) {
        int position = stockTipList.indexOf(r);
        if (position > -1) {
            stockTipList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
       // add(new StockTipList());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = stockTipList.size() - 1;
        StockTipList result = getItem(position);

        if (result != null) {
            stockTipList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public StockTipList getItem(int position) {
        return stockTipList.get(position);
    }

    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(stockTipList.size() - 1);

        if (errorMsg != null) this.errorMsg = errorMsg;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ItemTipslistHomeBinding itemView;

        public ViewHolder(ItemTipslistHomeBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
            itemView.executePendingBindings();
        }

        public ItemTipslistHomeBinding getItemView() {
            return itemView;
        }
    }

    protected class LoadingVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);

            mProgressBar = (ProgressBar) itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = (ImageButton) itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = (TextView) itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = (LinearLayout) itemView.findViewById(R.id.loadmore_errorlayout);

            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:

                    showRetry(false, null);
                    mCallback.retryPageLoad();

                    break;
            }
        }
    }
}
