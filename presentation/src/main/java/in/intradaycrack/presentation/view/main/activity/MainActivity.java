package in.intradaycrack.presentation.view.main.activity;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import java.util.List;

import javax.inject.Inject;

import in.intradaycrack.presentation.R;
import in.intradaycrack.presentation.databinding.TestLayoutBinding;
import in.intradaycrack.presentation.AndroidApplication;
import in.intradaycrack.presentation.Interface.ClearOperation;
import in.intradaycrack.presentation.base.BaseActivity;
import in.intradaycrack.presentation.customwidgets.ProgressController;
import in.intradaycrack.presentation.internal.di.components.DaggerMainAcitivityComponent;
import in.intradaycrack.presentation.internal.di.module.MainAcitivityModule;
import in.intradaycrack.presentation.view.main.mvp.MainActivityContractor;
import in.intradaycrack.presentation.view.main.mvp.MainPresenter;

public class MainActivity extends BaseActivity implements MainActivityContractor.View, ClearOperation {
    @Inject
    MainPresenter presenter;
    Activity activity;
    TestLayoutBinding AMB;
    ProgressController progressController;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = MainActivity.this;
        AMB = DataBindingUtil.setContentView(activity, R.layout.test_layout);
        DaggerMainAcitivityComponent.builder().applicationComponent(AndroidApplication.getAndroidApplication().getApplicationComponent()).mainAcitivityModule(new MainAcitivityModule(MainActivity.this)).build().inject(MainActivity.this);
        progressController = new ProgressController(AMB.getRoot(), MainActivity.this, MainActivity.this);
        setSupportActionBar(AMB.toolBar.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        AMB.toolBar.toolbarTitle.setText(R.string.app_name);
        presenter.requestToApi();
    }

    @Override
    public void showProgressbar() {
        progressController.showProgress();
    }

    @Override
    public void hideProgressBar() {
        progressController.onSuccess();
    }

    @Override
    public void showMessage(int drawable, int TMainMessage, int TSubMessage, int Case) {
        switch (Case) {
            case 0:
                progressController.setMessage(drawable, TMainMessage, TSubMessage, false, 0, null);
                break;
            case 1:
                progressController.setMessage(drawable, TMainMessage, TSubMessage, true, R.string.try_again, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        presenter.requestToApi();
                    }
                });
                break;
            default:
                progressController.setMessage(drawable, TMainMessage, TSubMessage, false, 0, null);
        }
    }

    @Override
    public void showError(String Error) {
        progressController.setError(Error);
    }

    @Override
    public void showErrorWithRetry(String Error, final int Case) {
        progressController.setError(Error, new ClearOperation() {
            @Override
            public void clear() {
                switch (Case) {
                    case 1:
                        presenter.requestToApi();
                        break;
                }
            }
        });
    }

    @Override
    public void setDisplay(List<String> listOfResult) {
        AMB.print.setText(listOfResult.toString());
    }

    @Override
    public void clear() {

    }
}
