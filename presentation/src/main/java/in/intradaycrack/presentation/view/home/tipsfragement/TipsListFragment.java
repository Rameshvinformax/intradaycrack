package in.intradaycrack.presentation.view.home.tipsfragement;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import in.intradaycrack.domain.entity.StockTipList;
import in.intradaycrack.presentation.R;
import in.intradaycrack.presentation.databinding.FragmentTipsListBinding;
import in.intradaycrack.domain.entity.StockTipListDomainEntity;
import in.intradaycrack.presentation.AndroidApplication;
import in.intradaycrack.presentation.Interface.ClearOperation;
import in.intradaycrack.presentation.base.BaseFragment;
import in.intradaycrack.presentation.customwidgets.ProgressController;
import in.intradaycrack.presentation.internal.di.components.DaggerTipListFragmentComponent;
import in.intradaycrack.presentation.internal.di.module.TipListFragmentModule;
import in.intradaycrack.presentation.view.home.tipsfragement.adapter.PaginationAdapterCallback;
import in.intradaycrack.presentation.view.home.tipsfragement.adapter.PaginationScrollListener;
import in.intradaycrack.presentation.view.home.tipsfragement.adapter.TipsListAdapter;
import in.intradaycrack.presentation.view.home.tipsfragement.mvp.TipsListContractor;
import in.intradaycrack.presentation.view.home.tipsfragement.mvp.TipsListPresenter;

public class TipsListFragment extends BaseFragment implements TipsListContractor.View, ClearOperation, PaginationAdapterCallback {

    FragmentTipsListBinding binding;
    ProgressController progressController;
    String disclimer = "";
    @Inject
    TipsListPresenter presenter;
    List<StockTipList> listOfTipsResult = new ArrayList<>();
    TipsListAdapter tipsListAdapter;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    // limiting to 5 for this tutorial, since total pages in actual API is very large. Feel free to modify.
    // private int TOTAL_PAGES = 5;
    boolean finalPage = true;
    private static final int PAGE_START = 1;
    private int currentPage = PAGE_START;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_tips_list, container, false);
        progressController = new ProgressController(binding.getRoot(), TipsListFragment.this, (AppCompatActivity) getActivity());
        DaggerTipListFragmentComponent.builder().applicationComponent(AndroidApplication.getAndroidApplication().getApplicationComponent()).tipListFragmentModule(new TipListFragmentModule(TipsListFragment.this)).build().inject(TipsListFragment.this);
        setView();
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Latest Tips");
    }

    void setView() {
        Bundle bundle = getActivity().getIntent().getExtras();
        disclimer = bundle.getString("disclimer");
        presenter.requestToApi(true, currentPage);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.scrollToPositionWithOffset(listOfTipsResult.size(), binding.recyclerviewTipslist.getPaddingBottom());
        binding.recyclerviewTipslist.setLayoutManager(linearLayoutManager);
        tipsListAdapter = new TipsListAdapter(listOfTipsResult, this, getContext(), disclimer);
        binding.recyclerviewTipslist.setAdapter(tipsListAdapter);
        binding.recyclerviewTipslist.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
                presenter.requestToApi(false, currentPage);
            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }

    @Override
    public void clear() {

    }

    @Override
    public void showProgressbar() {
        progressController.showProgress();
    }

    @Override
    public void hideProgressBar() {
        progressController.onSuccess();
    }

    @Override
    public void showMessage(int drawable, int TMainMessage, int TSubMessage, int Case) {
        switch (Case) {
            case 0:
                progressController.setMessage(drawable, TMainMessage, TSubMessage, false, 0, null);
                break;
            case 1:
                progressController.setMessage(drawable, TMainMessage, TSubMessage, true, R.string.try_again, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        presenter.requestToApi(true, 1);
                    }
                });
                break;
            default:
                progressController.setMessage(drawable, TMainMessage, TSubMessage, false, 0, null);
        }
    }

    @Override
    public void showError(String Error) {
        progressController.setError(Error);
    }

    @Override
    public void showErrorWithRetry(String Error, final int Case) {
        progressController.setError(Error, new ClearOperation() {
            @Override
            public void clear() {
                switch (Case) {
                    case 1:
                        presenter.requestToApi(true, 1);
                        break;
                }
            }
        });
    }


    @Override
    public void setDisplay(boolean showPrgFirstTime, StockTipListDomainEntity listOfResult) {
        /*this.listOfTipsResult = listOfResult.getStockTipList();
        tipsListAdapter = new TipsListAdapter(listOfTipsResult, this, getContext(), disclimer);
        binding.recyclerviewTipslist.setAdapter(tipsListAdapter);*/
        tipsListAdapter.addAll(listOfResult.getStockTipList());
        if (listOfResult.getStockTipList().size() < 10) {
            finalPage = false;
        }
        if (finalPage) tipsListAdapter.addLoadingFooter();
        else isLastPage = true;
        if (!showPrgFirstTime) {
            tipsListAdapter.removeLoadingFooter();
            isLoading = false;
        }
    }

    @Override
    public void retryPageLoad() {
        presenter.requestToApi(false, currentPage);
    }
}

