package in.intradaycrack.presentation.view.home.tipsfragement.mvp;

import java.util.List;

import in.intradaycrack.domain.entity.StockTipListDomainEntity;
import in.intradaycrack.domain.interactor.TipsListUseCase;
import in.intradaycrack.presentation.base.BasePresenter;
import in.intradaycrack.presentation.base.BaseView;

/**
 * Created by Admin on 09-11-2017.
 */

public interface TipsListContractor {

    interface View extends BaseView {
        void setDisplay(boolean showPrgFirstTime,StockTipListDomainEntity listOfResult);
    }

    interface Model {
        void request(TipsListUseCase tipsListUseCase,int pageNo,boolean showProgress);
    }

    interface Presenter extends BasePresenter {
        void requestToApi(boolean showProgress, int pageNo);

        void responseFromApi(boolean showProgress, StockTipListDomainEntity listOfResult);
    }
}
