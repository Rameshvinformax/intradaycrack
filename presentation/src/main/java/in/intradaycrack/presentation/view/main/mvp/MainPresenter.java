package in.intradaycrack.presentation.view.main.mvp;

import android.content.Context;

import java.util.List;

import javax.inject.Inject;

import in.intradaycrack.domain.interactor.MainActivityUseCase;

/**
 * Created by Admin on 09-11-2017.
 */

public class MainPresenter implements MainActivityContractor.Presenter {

    Context context;
    MainModel model;
    MainActivityUseCase mainActivityUseCase;
    MainActivityContractor.View view;

    @Inject
    public MainPresenter(MainActivityUseCase mainActivityUseCase, Context context, MainActivityContractor.View view) {
        this.mainActivityUseCase = mainActivityUseCase;
        this.context = context;
        this.view = view;
        this.model = new MainModel(MainPresenter.this, context);
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void passErrorAndCase(String Error, int Case) {
    }

    @Override
    public void passMessage(int drawable, int TMainMessage, int TSubMessage, int Case) {
        view.showMessage(drawable, TMainMessage, TSubMessage, Case);
    }

    @Override
    public void requestToApi() {
        view.showProgressbar();
        model.request(mainActivityUseCase);
    }

    @Override
    public void responseFromApi(List<String> listOfResult) {
        view.hideProgressBar();
        view.setDisplay(listOfResult);
    }
}
