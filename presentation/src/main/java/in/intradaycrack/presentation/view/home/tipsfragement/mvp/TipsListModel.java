package in.intradaycrack.presentation.view.home.tipsfragement.mvp;

import android.content.Context;

import in.intradaycrack.domain.entity.StockTipListDomainEntity;
import in.intradaycrack.domain.interactor.DefaultObserver;
import in.intradaycrack.domain.interactor.TipsListUseCase;
import in.intradaycrack.presentation.R;
import in.intradaycrack.presentation.exception.ErrorMessageFactory;

/**
 * Created by Admin on 09-11-2017.
 */

public class TipsListModel implements TipsListContractor.Model {

    TipsListContractor.Presenter presenter;
    Context context;
    String TAG = TipsListModel.class.getName();
    int switchCase = 0;

    public TipsListModel(TipsListContractor.Presenter presenter, Context context) {
        this.presenter = presenter;
        this.context = context;
    }

    @Override
    public void request(TipsListUseCase tipsListUseCase,int pageNo,final boolean showProgress) {
        switchCase = 1;
        tipsListUseCase.execute(new DefaultObserver<StockTipListDomainEntity>() {
            @Override
            public void onNext(StockTipListDomainEntity list) {
                if (list!=null&&list.getStatus().equals("1")&&list.getStockTipList().size()>0) {
                    presenter.responseFromApi(showProgress,list);
                } else {
                    presenter.passErrorAndCase(context.getString(R.string.empty_stocklist), switchCase);
                }
            }

            @Override
            public void onError(Throwable exception) {
                ErrorMessageFactory.exceptionCatcher(exception, presenter, switchCase);
            }
        }, pageNo);
    }
}
