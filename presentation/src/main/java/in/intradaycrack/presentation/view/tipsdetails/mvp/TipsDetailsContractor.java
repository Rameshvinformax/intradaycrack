package in.intradaycrack.presentation.view.tipsdetails.mvp;

import in.intradaycrack.domain.entity.StatusAndMessageDomainResponse;
import in.intradaycrack.domain.interactor.TipReadStatusUseCase;
import in.intradaycrack.presentation.base.BasePresenter;
import in.intradaycrack.presentation.base.BaseView;

/**
 * Created by Admin on 14-12-2017.
 */

public interface TipsDetailsContractor {

    interface View extends BaseView {
        void setDisplay(StatusAndMessageDomainResponse response);
    }

    interface Model {
        void request(TipReadStatusUseCase tipReadStatusUseCase, String tipID);
    }

    interface Presenter extends BasePresenter {
        void requestToApi(String tipID);

        void responseFromApi(StatusAndMessageDomainResponse response);
    }
}
