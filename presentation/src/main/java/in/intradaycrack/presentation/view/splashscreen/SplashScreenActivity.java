package in.intradaycrack.presentation.view.splashscreen;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;

import com.google.firebase.iid.FirebaseInstanceId;

import javax.inject.Inject;

import in.intradaycrack.domain.entity.StatusAndMessageDomainResponse;
import in.intradaycrack.presentation.AndroidApplication;
import in.intradaycrack.presentation.R;
import in.intradaycrack.presentation.base.BaseActivity;
import in.intradaycrack.presentation.databinding.ActivityMainBinding;
import in.intradaycrack.presentation.databinding.ActivitySplashScreenBinding;
import in.intradaycrack.presentation.fcm.FirebaseToken;
import in.intradaycrack.presentation.internal.di.components.DaggerSplashScreenComponent;
import in.intradaycrack.presentation.internal.di.module.SplashScreenModule;
import in.intradaycrack.presentation.utility.Connectivity;
import in.intradaycrack.presentation.utility.Utils;
import in.intradaycrack.presentation.view.home.activity.MainActivity;
import in.intradaycrack.presentation.view.splashscreen.mvp.SplashActivityContractor;
import in.intradaycrack.presentation.view.splashscreen.mvp.SplashActivityPresenter;

public class SplashScreenActivity extends BaseActivity implements SplashActivityContractor.View {

    private TelephonyManager telephonyManager;
    private String IMEI, versionCode;
    ActivitySplashScreenBinding binding;
    @Inject
    SplashActivityPresenter presenter;
    StatusAndMessageDomainResponse response;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(SplashScreenActivity.this, R.layout.activity_splash_screen);
        DaggerSplashScreenComponent.builder().applicationComponent(AndroidApplication.getAndroidApplication().getApplicationComponent()).splashScreenModule(new SplashScreenModule(SplashScreenActivity.this)).build().inject(SplashScreenActivity.this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Utils.checkAndRequestPermissions(SplashScreenActivity.this)) {
                    getIMEI();
                }
            }
        }, 1000);

    }

    public void getIMEI() {
        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        IMEI = telephonyManager.getDeviceId();
        proceedAuth();
    }

    public void proceedAuth() {
        if (Connectivity.isConnected(SplashScreenActivity.this)) {
            presenter.requestToApi(IMEI, FirebaseInstanceId.getInstance().getToken());
        } else {
            showNetworkError();
        }
    }

    public void showNetworkError() {

        Utils.showAlertOk(SplashScreenActivity.this, getString(R.string.no_internet), getString(R.string.no_internet_sub_msg), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
    }

    void navigateToHomeActivity() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.putExtra("disclimer", response.getDisclimer());
        startActivity(intent);
        finish();
    }

    public void showOrHideProgress(boolean status) {
        binding.progress.setVisibility(status ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (Utils.checkAndRequestPermissions(SplashScreenActivity.this)) {
            getIMEI();
        }
    }

    @Override
    public void showProgressbar() {
        showOrHideProgress(true);
    }

    @Override
    public void hideProgressBar() {
        showOrHideProgress(false);
    }

    @Override
    public void showMessage(int drawable, int TMainMessage, int TSubMessage, int Case) {

    }

    @Override
    public void showError(String Error) {

    }

    @Override
    public void showErrorWithRetry(String Error, int Case) {
        this.response = response;
    }

    @Override
    public void setDisplay(StatusAndMessageDomainResponse response) {
        this.response = response;
        if (response != null && response.getVersion() != null) {
            checkForUpdate();
        }
    }

    public void checkForUpdate() {
        validateAppVersion();
        if (response.getVersion() != null && versionCode != null) {
            if (!response.getVersion().equals(versionCode)) {
                updateRequest();
            } else {
                navigateToHomeActivity();
            }
        }
    }

    private void validateAppVersion() {
        try {
            PackageManager manager = getPackageManager();
            PackageInfo info = manager.getPackageInfo(getPackageName(), 0);
            versionCode = info.versionName;
        } catch (Exception e) {
            Log.d("VerError", "ValidateAppVersion: error " + e.getMessage());
        }

    }

    public void updateRequest() {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("New version available")
                .setMessage("Please, update app to new version to continue reposting.")
                .setPositiveButton("Update",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                redirectStore();
                            }
                        }).setNegativeButton("No, thanks",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                navigateToHomeActivity();
                            }
                        }).create();
        dialog.setCancelable(false);
        dialog.show();
    }
}
