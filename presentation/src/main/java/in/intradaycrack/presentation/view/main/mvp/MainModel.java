package in.intradaycrack.presentation.view.main.mvp;

import android.content.Context;

import java.util.List;

import in.intradaycrack.presentation.R;
import in.intradaycrack.domain.interactor.DefaultObserver;
import in.intradaycrack.domain.interactor.MainActivityUseCase;
import in.intradaycrack.presentation.exception.ErrorMessageFactory;
import in.intradaycrack.presentation.view.main.mvp.MainActivityContractor.Model;

/**
 * Created by Admin on 09-11-2017.
 */

public class MainModel implements Model {

    MainActivityContractor.Presenter presenter;
    Context context;
    String TAG = MainModel.class.getName();
    int switchCase = 0;

    public MainModel(MainActivityContractor.Presenter presenter, Context context) {
        this.presenter = presenter;
        this.context = context;
    }

    @Override
    public void request(MainActivityUseCase mainActivityUseCase) {
        switchCase = 1;
        mainActivityUseCase.execute(new DefaultObserver<List<String>>() {
            @Override
            public void onNext(List<String> list) {
                if (list.size() == 0) {
                    presenter.passErrorAndCase(context.getString(R.string.app_name), switchCase);
                } else {
                    presenter.responseFromApi(list);
                }
            }

            @Override
            public void onError(Throwable exception) {
                ErrorMessageFactory.exceptionCatcher(exception, presenter, switchCase);
            }
        }, null);
    }
}
