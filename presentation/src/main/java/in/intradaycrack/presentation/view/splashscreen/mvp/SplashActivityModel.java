package in.intradaycrack.presentation.view.splashscreen.mvp;

import android.content.Context;

import in.intradaycrack.domain.entity.StatusAndMessageDomainResponse;
import in.intradaycrack.domain.interactor.DefaultObserver;
import in.intradaycrack.domain.interactor.SplashScreenUseCase;
import in.intradaycrack.presentation.R;
import in.intradaycrack.presentation.exception.ErrorMessageFactory;

/**
 * Created by Admin on 14-12-2017.
 */

public class SplashActivityModel implements SplashActivityContractor.Model {

    SplashActivityContractor.Presenter presenter;
    Context context;
    String TAG = SplashActivityModel.class.getName();
    int switchCase = 0;

    public SplashActivityModel(SplashActivityContractor.Presenter presenter, Context context) {
        this.presenter = presenter;
        this.context = context;
    }

    @Override
    public void request(String imeiNo, String token, SplashScreenUseCase splashScreenUseCase) {
        switchCase = 1;
        splashScreenUseCase.execute(new DefaultObserver<StatusAndMessageDomainResponse>() {
            @Override
            public void onNext(StatusAndMessageDomainResponse response) {
                if (response != null && response.getStatus().equals("1")) {
                    presenter.responseFromApi(response);
                } else {
                    presenter.passErrorAndCase(response.getMessage(), switchCase);
                }
            }

            @Override
            public void onError(Throwable exception) {
                ErrorMessageFactory.exceptionCatcher(exception, presenter, switchCase);
            }
        }, new SplashScreenUseCase.Params(imeiNo, token));
    }

}
